/*
 * Copyright (c) 2013, Nick Gravelyn.
 *
 * This software is provided 'as-is', without any express or implied
 * warranty. In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 *    1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 *
 *    2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 *
 *    3. This notice may not be removed or altered from any source
 *    distribution.
 */

using UnityEngine;
using UnityEditor;
using System;
using System.Reflection;
using System.Collections;


[CustomEditor(typeof(GParticleExtend))]
public class GParticleExtendEditor : Editor {
	enum HandleMode{
		MainHandle, Position, Angle, Dimension,
	}HandleMode handleMode;

	GParticleExtend particle;

	private bool pref_IncludeChildren = true;

	//Scale
	private float ScalingValue = 2.0f;
	
	//Delay
	private float DelayValue = 1.0f;

	//Duration
	private float DurationValue = 5.0f;

	bool basicFoldout = false;
	bool childrenFoldout = false;

	private string[] _sortingLayerNames;

		float processTimer;

	void OnEnable()  {
	    var internalEditorUtilityType = Type.GetType("UnityEditorInternal.InternalEditorUtility, UnityEditor");
	    var sortingLayersProperty = internalEditorUtilityType.GetProperty("sortingLayerNames", BindingFlags.Static | BindingFlags.NonPublic);
	    _sortingLayerNames = sortingLayersProperty.GetValue(null, new object[0]) as string[];

		pref_IncludeChildren = true;
		particle = (GParticleExtend)target;

		basicFoldout = Boolean.Parse( PlayerPrefs.GetString("GParticleEditorBasicFoldout","false") );
		childrenFoldout = Boolean.Parse( PlayerPrefs.GetString("GParticleEditorChildrenFoldout","false") );

//		particle.Stop(false);
	}

	public String[] GetLayerNames(){
		ArrayList layerNames = new ArrayList ();

		for( int i = 0; i < 32; i++){
			string name = LayerMask.LayerToName(i);
			if( name.Length > 0 ){
				layerNames.Add( name );
			}
		}

		return layerNames.ToArray (typeof(string)) as string[];
	}
	void PlaySimulate(){
		particle.gameObject.SetActive(true);
		particle.Play();

		InitSimulateDeltaTime();
	}
	void StopSimulate(){

		particle.Stop();

	}
	public override void OnInspectorGUI() {

//			DrawDefaultInspector();

		if( GUILayout.Button("Load Particle Object ") ){
			particle.LoadSubParticleSystem();
		}
		
		if( GUILayout.Button("All Copy From Main ") ){
			particle.AllCopyFromMain();
		}

		EditorGUILayout.BeginHorizontal();

		if( GUILayout.Button("Apply") ){

			GameObject root = PrefabUtility.FindPrefabRoot( particle.gameObject );

			PrefabUtility.ReplacePrefab( root, PrefabUtility.GetPrefabParent( root ));
			
			PrefabUtility.RevertPrefabInstance( root );
		}


		EditorGUILayout.EndHorizontal();

		EditorGUILayout.Space();

		if(particle == null || particle.subCount == 0 ) return;

		EditorGUILayout.BeginHorizontal();

		GUILayout.Label(particle.main.name + "(s) : " );
		
		if(GUILayout.Button("Play", EditorStyles.miniButtonLeft, GUILayout.Width(50f)))
		{
			PlaySimulate();
		}
		if(GUILayout.Button("Pause", EditorStyles.miniButtonMid, GUILayout.Width(50f)))
		{
			particle.Pause();
		}
		if(GUILayout.Button("Stop", EditorStyles.miniButtonMid, GUILayout.Width(50f)))
		{
			StopSimulate();
		}
		if(GUILayout.Button("Clear", EditorStyles.miniButtonRight, GUILayout.Width(50f)))
		{
			particle.Clear();
		}

		EditorGUILayout.EndHorizontal();
		float tScale = EditorGUILayout.Slider("Time Scale : ",particle.timeScale , 0.01f,3f);

		if( tScale != particle.timeScale ){
			particle.timeScale = tScale;
		}


		particle.main.ps.playOnAwake = EditorGUILayout.Toggle("Play On Awake", particle.main.ps.playOnAwake);


		particle.useCameraLook = EditorGUILayout.Toggle("Use Camera Look : ", particle.useCameraLook);
		particle.useActiveOffAfterStop = EditorGUILayout.Toggle("Use Active Off After Stop : ", particle.useActiveOffAfterStop);
		particle.useDestroyAfterStop = EditorGUILayout.Toggle("Use Destroy After Stop : ", particle.useDestroyAfterStop);

		if( ( particle.useDestroyAfterStop || particle.useActiveOffAfterStop)){
			particle.useDestroyAfterStop = particle.useActiveOffAfterStop = false;
			EditorUtility.DisplayDialog("Check","You Can't check with GBullet Components\nthis variable is only false","OK");
		}
		//Separator
		EditorGUILayout.Separator();
		// #######################################################################################
		// Quick Editor
		// #######################################################################################
		EditorGUI.BeginChangeCheck();
		basicFoldout = EditorGUILayout.Foldout(basicFoldout, "QUICK EDIT");
		if(basicFoldout)
		{
			//----------------------------------------------------------------
			
			GUILayout.BeginHorizontal();
			if(GUILayout.Button(new GUIContent("Scale Size", "Changes the size of the Particle System(s) and other values accordingly (speed, gravity, etc.)"), GUILayout.Width(120)))
			{
				applyScale();
				PlaySimulate();
			}
			GUILayout.Label("Multiplier:",GUILayout.Width(110));
			ScalingValue = EditorGUILayout.FloatField(ScalingValue,GUILayout.Width(50));
			if(ScalingValue <= 0) ScalingValue = 0.1f;
			GUILayout.EndHorizontal();
			
			//----------------------------------------------------------------
			
			GUILayout.BeginHorizontal();
			if(GUILayout.Button(new GUIContent("Set Speed", "Changes the speed of the Particle System(s) (if you want quicker or longer effects, 100% = default speed)"), GUILayout.Width(120)))
			{
				particle.timeScale = particle.timeScale;
				PlaySimulate();
			}
			GUILayout.Label("Speed (%):",GUILayout.Width(110));
			particle.editPlaybackSpeedValue = EditorGUILayout.FloatField(particle.editPlaybackSpeedValue,GUILayout.Width(50));
			if(particle.editPlaybackSpeedValue < 0.1f) particle.editPlaybackSpeedValue = 0.1f;
			else if(particle.editPlaybackSpeedValue > 9999) particle.editPlaybackSpeedValue = 9999;
			GUILayout.EndHorizontal();
			
			//----------------------------------------------------------------
			
			GUILayout.BeginHorizontal();
			if(GUILayout.Button(new GUIContent("Set Duration", "Changes the duration of the Particle System(s)"), GUILayout.Width(120)))
			{
				applyDuration();
				PlaySimulate();
			}
			GUILayout.Label("Duration (sec):",GUILayout.Width(110));
			DurationValue = EditorGUILayout.FloatField(DurationValue,GUILayout.Width(50));
			if(DurationValue < 0.1f) DurationValue = 0.1f;
			else if(DurationValue > 9999) DurationValue = 9999;
			GUILayout.EndHorizontal();
			
			//----------------------------------------------------------------
			
			GUILayout.BeginHorizontal();
			if(GUILayout.Button(new GUIContent("Set Delay", "Changes the delay of the Particle System(s)"), GUILayout.Width(120)))
			{
				applyDelay();
				PlaySimulate();
			}
			GUILayout.Label("Delay :",GUILayout.Width(110));
			DelayValue = EditorGUILayout.FloatField(DelayValue,GUILayout.Width(50));
			if(DelayValue < 0.0f) DelayValue = 0.0f;
			else if(DelayValue > 9999f) DelayValue = 9999f;
			GUILayout.EndHorizontal();
			
			//----------------------------------------------------------------
			
			GUILayout.Space(2);
			
			GUILayout.BeginHorizontal();
			if(GUILayout.Button(new GUIContent("Loop", "Loop the effect (might not work properly on some effects such as explosions)"), EditorStyles.miniButtonLeft))
			{
				loopEffect(true);
			}
			if(GUILayout.Button(new GUIContent("Unloop", "Remove looping from the effect"), EditorStyles.miniButtonRight))
			{
				loopEffect(false);
			}
			GUILayout.EndHorizontal();
			
			GUILayout.Space(2);
			
			//----------------------------------------------------------------
			
		}


		// #######################################################################################
		// Childrens
		// #######################################################################################

		childrenFoldout = EditorGUILayout.Foldout(childrenFoldout, "Children EDIT");
		if(childrenFoldout)
		{
			EditorGUI.indentLevel += 1;
			for( int i = 0; i < particle.subCount; i++){

				GParticleExtend.ParticleInfo pinfo = particle.sub[i];

				if( pinfo.ps == null ){
					particle.LoadSubParticleSystem();
					return;
				}
				// Get the renderer from the target object
				Renderer renderer = pinfo.ps.renderer;
				
				// If there is no renderer, we can't do anything
				if (renderer)
				{
					string mainStr = "";

					if( pinfo.ps.gameObject == particle.main.ps.gameObject ) mainStr = " (Main)";
					pinfo.ps = EditorGUILayout.ObjectField( pinfo.name + mainStr, pinfo.ps, typeof( ParticleSystem), true ) as ParticleSystem;

					EditorGUI.indentLevel += 1;

					string layerName = renderer.sortingLayerName;


					int layerIndex = Array.IndexOf(_sortingLayerNames, layerName);
					int newLayerIndex = EditorGUILayout.Popup("Sorting Layer", layerIndex, _sortingLayerNames);

					if( newLayerIndex > 0 )
						pinfo.layerName = _sortingLayerNames[newLayerIndex];
					else if( i > 0 )
						particle.AllCopyFromMain();

					int newSortingLayerOrder = EditorGUILayout.IntField("Sorting Layer Order", renderer.sortingOrder);
					if (newSortingLayerOrder != renderer.sortingOrder) {
						Undo.RecordObject(renderer, "Edit Sorting Order");
						renderer.sortingOrder = newSortingLayerOrder;
						EditorUtility.SetDirty(renderer);
					}

					EditorGUI.indentLevel -= 1;
				}

				EditorGUILayout.Space();
			}
			EditorGUI.indentLevel -= 1;
		}

		// #######################################################################################
		// Childrens
		// #######################################################################################

		//update and redraw:
		if(GUI.changed){
			EditorUtility.SetDirty(particle);
//			particle.gameObject.SetActive( boxColliderFoldout );

			PlayerPrefs.SetString("GParticleEditorBasicFoldout",basicFoldout.ToString());
			PlayerPrefs.SetString("GParticleEditorChildrenFoldout",childrenFoldout.ToString());
//			PlayerPrefs.SetString("GParticleEditorBoxColliderFoldout",boxColliderFoldout.ToString());
		}
	}

	void InitSimulateDeltaTime(){
		timeTemp = Time.realtimeSinceStartup;
	}

	float timeTemp;
	float simulateDeltaTime{get{ 

			float t = Time.realtimeSinceStartup;

			float r = t - timeTemp;

			timeTemp = t;

			return r;
		} } 

	void OnSceneGUI(){
		
		if(particle == null || particle.subCount == 0 ) return;


		if(GUI.changed)
		{
			HandleUtility.Repaint();
			EditorUtility.SetDirty(particle);	
		}
	}
	/*Event _event = Event.current;

		// Interception Auto Select Object on Scene view!!!!
		if (_event.type == EventType.Layout){				
			
			if( handleMode == HandleMode.MainHandle )
			{
			}
			else{
				Tools.current = Tool.None;
			}
		}
		
		SceneView.currentDrawingSceneView.rotation = new Quaternion(0,0,0,1);
		
		if( _event.keyCode == KeyCode.Q ){
			handleMode = HandleMode.MainHandle;
		}else if( _event.keyCode == KeyCode.Z ){
			Tools.current = Tool.None;
			handleMode = HandleMode.Position;
			particle.boxCollider.enabled = true;
			
			particle.gameObject.SetActive( true );
		}else if( _event.keyCode == KeyCode.X ){
			Tools.current = Tool.None;
			handleMode = HandleMode.Dimension;	
			particle.boxCollider.enabled = true;
			particle.gameObject.SetActive( true );
		}

		Vector3 tPos = particle.boxCollider.center + particle.transform.position;
		
		switch( handleMode ){
		case HandleMode.Position:
			tPos = Handles.PositionHandle( tPos,  particle.boxCollider.transform.rotation );
			particle.boxCollider.center = tPos - particle.boxCollider.transform.position;
			break;
		case HandleMode.Dimension:
			Vector3 pos1 = Camera.current.transform.position;
			Vector3 pos2 = particle.boxCollider.transform.position;
			pos1.x = 0f;pos1.y = 0;
			pos2.x = 0f;pos2.y = 0;

			float size = Vector3.Distance( pos1, pos2);
			particle.boxCollider.size = Handles.ScaleHandle( particle.boxCollider.size, tPos, particle.boxCollider.transform.rotation, size/20f);//.PositionHandle( tPos, Quaternion.identity );
			break;
		}

		

		if(GUI.changed)
		{
			HandleUtility.Repaint();
			EditorUtility.SetDirty(particle);	
		}
	}

*/
	//Set Duration
	private void applyDuration()
	{
		foreach(GameObject go in Selection.gameObjects)
		{
			//Scale Shuriken Particles Values
			ParticleSystem[] systems;
			if(pref_IncludeChildren)
				systems = go.GetComponentsInChildren<ParticleSystem>(true);
			else
				systems = go.GetComponents<ParticleSystem>();
			
			foreach(ParticleSystem ps in systems)
			{
				SerializedObject so = new SerializedObject(ps);
				so.FindProperty("lengthInSec").floatValue = DurationValue;
				so.ApplyModifiedProperties();
			}
		}
	}
	
	//Change delay
	private void applyDelay()
	{
		foreach(GameObject go in Selection.gameObjects)
		{
			ParticleSystem[] systems;
			if(pref_IncludeChildren)
				systems = go.GetComponentsInChildren<ParticleSystem>(true);
			else
				systems = go.GetComponents<ParticleSystem>();
			
			//Scale Lifetime
			foreach(ParticleSystem ps in systems)
			{
				ps.startDelay = DelayValue;
			}
		}
	}

	private void applyScale()
	{
		foreach(GameObject go in Selection.gameObjects)
		{
			//Scale Shuriken Particles Values
			ParticleSystem[] systems;
			if(pref_IncludeChildren)
				systems = go.GetComponentsInChildren<ParticleSystem>(true);
			else
				systems = go.GetComponents<ParticleSystem>();
			
			foreach(ParticleSystem ps in systems)
			{
				ScaleParticleValues(ps, go);
			}
			
			//Scale Lights' range
			Light[] lights = go.GetComponentsInChildren<Light>();
			foreach(Light light in lights)
			{
				light.range *= ScalingValue;
				light.transform.localPosition *= ScalingValue;
			}
		}
	}

	//Loop effects
	private void loopEffect(bool setLoop)
	{
		foreach(GameObject go in Selection.gameObjects)
		{
			//Scale Shuriken Particles Values
			ParticleSystem[] systems;
			if(pref_IncludeChildren)
				systems = go.GetComponentsInChildren<ParticleSystem>(true);
			else
				systems = go.GetComponents<ParticleSystem>();
			
			foreach(ParticleSystem ps in systems)
			{
				SerializedObject so = new SerializedObject(ps);
				so.FindProperty("looping").boolValue = setLoop;
				so.ApplyModifiedProperties();
			}
		}
	}

	//Scale System
	private void ScaleParticleValues(ParticleSystem ps, GameObject parent)
	{
		//Particle System
		ps.startSize *= ScalingValue;
		ps.gravityModifier *= ScalingValue;
		if(ps.startSpeed > 0.01f)
			ps.startSpeed *= ScalingValue;
		if(ps.gameObject != parent)
			ps.transform.localPosition *= ScalingValue;
		
		SerializedObject psSerial = new SerializedObject(ps);
		
		//Scale Emission Rate if set on Distance
		if(psSerial.FindProperty("EmissionModule.enabled").boolValue && psSerial.FindProperty("EmissionModule.m_Type").intValue == 1)
		{
			psSerial.FindProperty("EmissionModule.rate.scalar").floatValue /= ScalingValue;
		}
		
		//Scale Size By Speed Module
		if(psSerial.FindProperty("SizeBySpeedModule.enabled").boolValue)
		{
			psSerial.FindProperty("SizeBySpeedModule.range.x").floatValue *= ScalingValue;
			psSerial.FindProperty("SizeBySpeedModule.range.y").floatValue *= ScalingValue;
		}
		
		//Scale Velocity Module
		if(psSerial.FindProperty("VelocityModule.enabled").boolValue)
		{
			psSerial.FindProperty("VelocityModule.x.scalar").floatValue *= ScalingValue;
			IterateKeys(psSerial.FindProperty("VelocityModule.x.minCurve").animationCurveValue);
			IterateKeys(psSerial.FindProperty("VelocityModule.x.maxCurve").animationCurveValue);
			psSerial.FindProperty("VelocityModule.y.scalar").floatValue *= ScalingValue;
			IterateKeys(psSerial.FindProperty("VelocityModule.y.minCurve").animationCurveValue);
			IterateKeys(psSerial.FindProperty("VelocityModule.y.maxCurve").animationCurveValue);
			psSerial.FindProperty("VelocityModule.z.scalar").floatValue *= ScalingValue;
			IterateKeys(psSerial.FindProperty("VelocityModule.z.minCurve").animationCurveValue);
			IterateKeys(psSerial.FindProperty("VelocityModule.z.maxCurve").animationCurveValue);
		}
		
		//Scale Limit Velocity Module
		if(psSerial.FindProperty("ClampVelocityModule.enabled").boolValue)
		{
			psSerial.FindProperty("ClampVelocityModule.x.scalar").floatValue *= ScalingValue;
			IterateKeys(psSerial.FindProperty("ClampVelocityModule.x.minCurve").animationCurveValue);
			IterateKeys(psSerial.FindProperty("ClampVelocityModule.x.maxCurve").animationCurveValue);
			psSerial.FindProperty("ClampVelocityModule.y.scalar").floatValue *= ScalingValue;
			IterateKeys(psSerial.FindProperty("ClampVelocityModule.y.minCurve").animationCurveValue);
			IterateKeys(psSerial.FindProperty("ClampVelocityModule.y.maxCurve").animationCurveValue);
			psSerial.FindProperty("ClampVelocityModule.z.scalar").floatValue *= ScalingValue;
			IterateKeys(psSerial.FindProperty("ClampVelocityModule.z.minCurve").animationCurveValue);
			IterateKeys(psSerial.FindProperty("ClampVelocityModule.z.maxCurve").animationCurveValue);
			
			psSerial.FindProperty("ClampVelocityModule.magnitude.scalar").floatValue *= ScalingValue;
			IterateKeys(psSerial.FindProperty("ClampVelocityModule.magnitude.minCurve").animationCurveValue);
			IterateKeys(psSerial.FindProperty("ClampVelocityModule.magnitude.maxCurve").animationCurveValue);
		}
		
		//Scale Force Module
		if(psSerial.FindProperty("ForceModule.enabled").boolValue)
		{
			psSerial.FindProperty("ForceModule.x.scalar").floatValue *= ScalingValue;
			IterateKeys(psSerial.FindProperty("ForceModule.x.minCurve").animationCurveValue);
			IterateKeys(psSerial.FindProperty("ForceModule.x.maxCurve").animationCurveValue);
			psSerial.FindProperty("ForceModule.y.scalar").floatValue *= ScalingValue;
			IterateKeys(psSerial.FindProperty("ForceModule.y.minCurve").animationCurveValue);
			IterateKeys(psSerial.FindProperty("ForceModule.y.maxCurve").animationCurveValue);
			psSerial.FindProperty("ForceModule.z.scalar").floatValue *= ScalingValue;
			IterateKeys(psSerial.FindProperty("ForceModule.z.minCurve").animationCurveValue);
			IterateKeys(psSerial.FindProperty("ForceModule.z.maxCurve").animationCurveValue);
		}
		
		//Scale Shape Module
		if(psSerial.FindProperty("ShapeModule.enabled").boolValue)
		{
			psSerial.FindProperty("ShapeModule.boxX").floatValue *= ScalingValue;
			psSerial.FindProperty("ShapeModule.boxY").floatValue *= ScalingValue;
			psSerial.FindProperty("ShapeModule.boxZ").floatValue *= ScalingValue;
			psSerial.FindProperty("ShapeModule.radius").floatValue *= ScalingValue;
			
			//Create a new scaled Mesh if there is a Mesh reference
			//(ShapeModule.type 6 == Mesh)
			if(psSerial.FindProperty("ShapeModule.type").intValue == 6)
			{
				UnityEngine.Object obj = psSerial.FindProperty("ShapeModule.m_Mesh").objectReferenceValue;
				if(obj != null)
				{
					Mesh mesh = (Mesh)obj;
					string assetPath = AssetDatabase.GetAssetPath(mesh);
					string name = assetPath.Substring(assetPath.LastIndexOf("/")+1);
					
					//Mesh to use
					Mesh meshToUse = null;
					bool createScaledMesh = true;
					float meshScale = ScalingValue;
					
					//Mesh has already been scaled: extract scaling value and re-scale base effect
					if(name.Contains("(scaled)"))
					{
						string scaleStr = name.Substring(name.LastIndexOf("x")+1);
						scaleStr = scaleStr.Remove(scaleStr.IndexOf(" (scaled).asset"));
						
						float oldScale = float.Parse(scaleStr);
						if(oldScale != 0)
						{
							meshScale = oldScale * ScalingValue;
							
							//Check if there's already a mesh with the correct scale
							string unscaledName = assetPath.Substring(0, assetPath.LastIndexOf(" x"));
							assetPath = unscaledName;
							string newPath = assetPath + " x"+meshScale+" (scaled).asset";
							Mesh alreadyScaledMesh = (Mesh)AssetDatabase.LoadAssetAtPath(newPath, typeof(Mesh));
							if(alreadyScaledMesh != null)
							{
								meshToUse = alreadyScaledMesh;
								createScaledMesh = false;
							}
							else
								//Load original unscaled mesh
							{
								Mesh orgMesh = (Mesh)AssetDatabase.LoadAssetAtPath(assetPath, typeof(Mesh));
								if(orgMesh != null)
								{
									mesh = orgMesh;
								}
							}
						}
					}
					else
						//Verify if original mesh has already been scaled to that value
					{
						string newPath = assetPath + " x"+meshScale+" (scaled).asset";
						Mesh alreadyScaledMesh = (Mesh)AssetDatabase.LoadAssetAtPath(newPath, typeof(Mesh));
						if(alreadyScaledMesh != null)
						{
							meshToUse = alreadyScaledMesh;
							createScaledMesh = false;
						}
					}
					
					//Duplicate and scale mesh vertices if necessary
					if(createScaledMesh)
					{
						string newMeshPath = assetPath + " x"+meshScale+" (scaled).asset";
						meshToUse = (Mesh)AssetDatabase.LoadAssetAtPath(newMeshPath, typeof(Mesh));
						if(meshToUse == null)
						{
							meshToUse = DuplicateAndScaleMesh(mesh, meshScale);
							AssetDatabase.CreateAsset(meshToUse, newMeshPath);
						}
					}
					
					//Apply new Mesh
					psSerial.FindProperty("ShapeModule.m_Mesh").objectReferenceValue = meshToUse;
				}
			}
		}
		
		//Apply Modified Properties
		psSerial.ApplyModifiedProperties();
	}

	//Iterate and Scale Keys (Animation Curve)
	private void IterateKeys(AnimationCurve curve)
	{
		for(int i = 0; i < curve.keys.Length; i++)
		{
			curve.keys[i].value *= ScalingValue;
		}
	}

	//Create Scaled Mesh
	private Mesh DuplicateAndScaleMesh(Mesh mesh, float Scale)
	{
		Mesh scaledMesh = new Mesh();
		
		Vector3[] scaledVertices = new Vector3[mesh.vertices.Length];
		for(int i = 0; i < scaledVertices.Length; i++)
		{
			scaledVertices[i] = mesh.vertices[i] * Scale;
		}
		scaledMesh.vertices = scaledVertices;
		
		scaledMesh.normals = mesh.normals;
		scaledMesh.tangents = mesh.tangents;
		scaledMesh.triangles = mesh.triangles;
		scaledMesh.uv = mesh.uv;
		scaledMesh.uv2 = mesh.uv2;
		scaledMesh.colors = mesh.colors;
		
		return scaledMesh;
	}

}