using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace GPool{
	//GPool ==> GPPrefabFolders ==> GPPrefabs ==> createObjects
	public class MGParticle : MonoSingleton<MGParticle> {
				
		//##################################################################################
		// ObjectCollection 
		//
		//
		//##################################################################################
		[System.Serializable]	
		public class GPCollection{
			public List< GParticleExtend > objects = new List< GParticleExtend >();
			public GPool.GPPrefabInfo info;
			private int createID = 0;
			
			public GPCollection(GPPrefabInfo _info){
				info = _info;

				info.defaultCreateCount = Mathf.Max( PlayerPrefs.GetInt(info.name,0), info.defaultCreateCount);

				for ( int i = 0; i < info.defaultCreateCount ; i++){
					AddCreateObject( false , Vector3.up * 100f	 );
				}
			}
			
			
			public GParticleExtend AddCreateObject ( bool isActive, Vector3 pos){
				GameObject go = Instantiate( info.prefab,pos,Quaternion.identity ) as GameObject; //cerate

				GParticleExtend parti = go.GetComponent< GParticleExtend >();
				parti.Stop();
				parti.useActiveOffAfterStop = true;

				go.name = info.prefab.name + "_" +createID++;
				go.transform.parent = MGParticle.i.transform;
				go.gameObject.SetActive( isActive );

				objects.Add ( parti ); //add
				
				return parti;  //return
			}		
			
			public GParticleExtend GetObject( Vector3 pos){
				
				//미 사용중인 객체를 찾아 활성화 시키고 위치를 조정후 재생 한다.
				for ( int i = 0; i < objects.Count ; i++ ){
					if ( ! objects[ i ].gameObject.activeSelf && ! objects[ i ].gameObject.activeInHierarchy ){ 	//search
						objects[ i ].transform.position = pos; 			//positioning
						objects[ i ].gameObject.SetActive(true); 		//active
						return objects[ i ];
					}
				}
				
				//전부 사용중이어서 위 구문을 지나 친경우 새로 생성 시키고 리스트에 추가 한다.
				PlayerPrefs.SetInt(info.name,objects.Count + 1);
				return AddCreateObject(true, pos);
			}	
		}
		
		public GPool.PoolFolder[] folders;
		
		public Dictionary<string, GPCollection> collections = new Dictionary<string, GPCollection>();
	
		bool isInit = false;

		// test
		public void CreateObject(string key){
			PlayerPrefs.SetInt(collections[key].info.name,collections[key].objects.Count + 1);
			collections[key].AddCreateObject(true,Vector3.zero);
		}

		// Use this for initialization
		void Awake () {				
			CreateCollection();
			isInit = true;
		}
				
		public void RenamePrefabInfos(){
			for(int fid = 0; fid < folders.Length; fid++){
				for(int i = 0; i < folders[fid].prefabInfos.Length; i++){
					folders[fid].prefabInfos[i].name = folders[fid].prefabInfos[i].prefab.name + " (" + folders[fid].prefabInfos[i].defaultCreateCount + ")";
				}
			}
		}

		/// <summary>
		/// Renames this instance.
		/// 프리펩의 이름에 다가 현재 생성한 오브젝트의 갯수를 표기한다.
		/// </summary>
		public void LoadObjectCount(){
			for(int fid = 0; fid < folders.Length; fid++){
				for(int i = 0; i < folders[fid].prefabInfos.Length; i++){				
					folders[fid].prefabInfos[i].defaultCreateCount = PlayerPrefs.GetInt(folders[fid].prefabInfos[i].name,1);
				}
			}
		}

		public void SaveObjectCount(){
			for(int fid = 0; fid < folders.Length; fid++){
				for(int i = 0; i < folders[fid].prefabInfos.Length; i++){				
					PlayerPrefs.SetInt(folders[fid].prefabInfos[i].name, folders[fid].prefabInfos[i].defaultCreateCount);
				}
			}
		}
		
		public void CreateCollection(){
			
//			collections_debug.Clear();
			
			collections.Clear();
			
			for(int fid = 0; fid < folders.Length; fid++){
				foreach ( GPPrefabInfo info in folders[fid].prefabInfos ){
					GPCollection collection = new GPCollection( info );	
					
	//				collections_debug.Add( collection );
					collections.Add( info.prefab.name, collection );
				}
			}
		}
	
		public void Stop( GParticleExtend ps ){
			ps.transform.parent = transform;
			ps.Stop();
		}

		public bool CheckKey( string key ){
			if( ! collections.ContainsKey( key ) ){	
				print ( "Error : Pool Object Name : " + key );	
				return false;
			}else{
				return true;
			}
		}

		//외부 클래스에서 본 함수를 통해 미리 생성된 객체를 가져다가 사용한다.
		public GParticleExtend GetObject(string key ,Vector3 pos) 
		{
			return GetObject( key, transform, pos );
		}

		public GParticleExtend GetObject(string key, Transform parent ,Vector3 pos) 
		{
			if( ! isInit ) return null;
			if( ! CheckKey ( key ) ) return null;

			GParticleExtend ps = collections[ key ].GetObject(pos);

			ps.transform.parent = parent;

			return ps;
		}

		public void PlayOneShot(string key ,Vector3 pos) {
			if( ! isInit ) return;
			if( ! CheckKey ( key ) ) return;

			collections[ key ].GetObject(pos).Play();
		}

		public void PlayOneShot(string key ,Vector3 pos, bool isEnemy) {
			if( ! isInit ) return;
			if( ! CheckKey ( key ) ) return;

			GParticleExtend ps = collections[ key ].GetObject(pos);
			ps.isEnemy = isEnemy;
			ps.Play();
		}

		public void PlayOneShot(float startDelay,string key ,Vector3 pos) {	
			if( ! isInit ) return;
			if( ! CheckKey ( key ) ) return;

			collections[ key ].GetObject(pos).Play( startDelay );
		}

		public void PlayOneShot(float startDelay,string key ,Vector3 pos, bool isEnemy) {	
			if( ! isInit ) return;
			if( ! CheckKey ( key ) ) return;

			GParticleExtend ps = collections[ key ].GetObject(pos);
			ps.isEnemy = isEnemy;
			ps.Play( startDelay );
		}

		public void PlayOneShot(float startDelay, string key ,Vector3 pos, float stoptime) {	
			if( ! isInit ) return;
			if( ! CheckKey ( key ) ) return;

			GParticleExtend ps = collections[ key ].GetObject(pos);
			ps.Play();
			ps.AutoStop( stoptime );
		}

		public void PlayOneShot(float startDelay, string key ,Vector3 pos, float stoptime, bool isEnemy) {	
			if( ! isInit ) return;
			if( ! CheckKey ( key ) ) return;

			GParticleExtend ps = collections[ key ].GetObject(pos);
			ps.isEnemy = isEnemy;
			ps.Play();
			ps.AutoStop( stoptime );
		}

	}//End Class
}

