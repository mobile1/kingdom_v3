﻿using UnityEngine;
using System.Collections;

namespace GPool
{
	public abstract class Pool_Object : MonoBehaviour 
	{	
		public abstract void Reset();
		public abstract void MyDestroy();
	}
}