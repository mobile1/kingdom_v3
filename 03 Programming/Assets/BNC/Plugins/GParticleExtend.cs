/*
 * Copyright (c) 2013, Nick Gravelyn.
 *
 * This software is provided 'as-is', without any express or implied
 * warranty. In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 *    1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 *
 *    2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 *
 *    3. This notice may not be removed or altered from any source
 *    distribution.
 */

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent( typeof( ParticleSystem )) ]

// Component does nothing; editor script does all the magic
public class GParticleExtend : MonoBehaviour { 
	[System.Serializable]
	public class ParticleInfo{
		public string name;
		public ParticleSystem ps;
		public string layerName{ get{ return ps.renderer.sortingLayerName; } set{ ps.renderer.sortingLayerName = value; }}
		public int renderOrder{ get{ return ps.gameObject.renderer.sortingOrder; } set{ ps.gameObject.renderer.sortingOrder = value; }}

		public ParticleInfo( ParticleSystem _ps ){
			Setup ( _ps );
		}

		public void Setup( ParticleSystem _ps ){
			name = _ps.name;
			ps = _ps;
		}

//			public void SetActive( bool value ){
//				ps.enableEmission = value;
//			}
	}

	public int mainIdx = 0;
	public ParticleInfo main{ get{ return sub[ mainIdx]; }}
	public List< ParticleInfo > sub = new List<ParticleInfo>();
	public int subCount = 0;

	public float editPlaybackSpeedValue = 100f;
	public float timeScale{ 
		get{ return main.ps.playbackSpeed; }
		set{ 
			float val = value < 0 ? 0 : value;
			for( int i = 0; i < subCount; i++){
				sub[i].ps.playbackSpeed = (100f/ editPlaybackSpeedValue) * val;
			}
		}
	}

	public bool useCameraLook = true;
//	public BoxCollider boxCollider;
//	public float boxColliderDelayTime;
//	public float boxColliderTime;

//	public GPool.GBullet bullet;

	public bool useActiveOffAfterStop;
	public bool useDestroyAfterStop;
	
	public bool isEnemy;


	#region default

	Camera mainCamera;

	void Awake(){
		mainCamera = Camera.main;
	}

	void Start(){
		if( main.ps.playOnAwake ) Play ();
	}

	void OnDrawGizmosSelected(){
//		if( bullet == null ) return;
//
//		if(bullet.boxCollider.enabled ) Gizmos.color = Color.red;
//		else Gizmos.color = Color.green;
//
//		Gizmos.DrawWireCube( transform.position + bullet.boxCollider.center + (Vector3.forward * 2f), bullet.boxCollider.size );
	}

	#endregion
	public void LoadSubParticleSystem(){
		if( sub.Count > 0 ) sub.Clear();
		gameObject.SetActive(true);
		ParticleSystem[] pss = GetComponentsInChildren< ParticleSystem >();

		for( int i = 0;i < pss.Length; i++){
			sub.Add( new ParticleInfo( pss[i] ));
			
			if( pss[i].gameObject == gameObject ){
				mainIdx = i;
			}
		}
		subCount = sub.Count;
//		gameObject.SetActive(false);
	}

	public void AllCopyFromMain(){

		foreach( ParticleInfo pi in sub ){
			pi.layerName = main.layerName;
			pi.renderOrder = main.renderOrder;
		}
	}

//		public void SetAtive(bool value){
//			for( int i = 0; i < subCount; i++){
//				sub[i].SetActive( value );
//			}
//		}

	public void Clear(){
		for( int i = 0; i < subCount; i++){
			sub[i].ps.Clear();
		}
	}

	public void Stop(){
		StopAllCoroutines();

		for( int i = 0; i < subCount; i++){
			sub[i].ps.enableEmission = false;
			sub[i].ps.Clear();
			sub[i].ps.Stop();
		}

		if( useActiveOffAfterStop ) gameObject.SetActive( false );

		if( useDestroyAfterStop ) DestroyImmediate( gameObject );
	}

	public void Play(){
		gameObject.SetActive( true );
		Play (0f);
	}
	public void Play(float delaytime){
		StartCoroutine( PlayIE ( delaytime ) );
	}

	IEnumerator PlayIE(float delaytime){
		yield return new WaitForSeconds( delaytime );

		if( main.ps.isPlaying ) StopAllCoroutines();

		for( int i = 0; i < subCount; i++){
			sub[i].ps.enableEmission = true;
			sub[i].ps.Play();
		}

		if( ! main.ps.loop ) AutoStop( main.ps.duration );
	}


	void FixedUpdate(){
//		if( timeScale != KDI_IngameManager.i.playTimeScale)
//			timeScale = KDI_IngameManager.i.playTimeScale;

		if( mainCamera && useCameraLook ) transform.localEulerAngles = mainCamera.transform.localEulerAngles;
	}

	public void AutoStop( float time ){ StartCoroutine( AutoStopIE( time ) ); }
	IEnumerator AutoStopIE(float time){
		yield return new WaitForSeconds( time );
		Stop ();
	}

	public void Pause(){
		for( int i = 0; i < subCount; i++){
			sub[i].ps.Pause();
		}
	}

}

