﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class SkinData
{
    public KDIE_ATTACK_TYPE Attack_type;
    public SkeletonDataAsset SkeletonData;
    public string SkinCode;
    public string WeaponCode;
    public string IconName;
}

public class KDD_CharacterSkinData_Manager : MonoSingleton<KDD_CharacterSkinData_Manager>
{
    public List<SkinData> EnemyDataList = new List<SkinData>();
    public List<SkinData> GeneralsDataList = new List<SkinData>();


    public override void Init()
    {
        DontDestroyOnLoad(this);

        //SkeletonDataAsset SkeletonData = Instantiate(Resources.Load("SpineResource/Ghost/SkeletonAssetData/Ghost",typeof(SkeletonDataAsset))) as SkeletonDataAsset;     
        //string name = SkeletonData.name.Replace("(Clone)", "");
        //SkeletonData.name = name;
        //Debug.Log("Skeleton name = " + SkeletonData.name);
    }

    public SkeletonDataAsset GetCharacterSKData(int _ID)
    {
        if (_ID <= 0) _ID = 1;

        //if (character_type == KDIE_CHARACTER_TYPE.GENERAL_TYPE)
        {
            return GeneralsDataList[_ID - 1].SkeletonData;
        }
        //else
        //{
        //    return EnemyDataList[_ID - 1].SkeletonData;
        //}
    }

    public SkeletonDataAsset GetCharacterSKData(string SkinCode)
    {
        //if (character_type == KDIE_CHARACTER_TYPE.GENERAL_TYPE)
        {
            for (int i = 0; i < GeneralsDataList.Count; i++)
            {
                if (GeneralsDataList[i].SkinCode == SkinCode)
                    return GeneralsDataList[i].SkeletonData;
            }
        }
        /*
        else
        {
            for (int i = 0; i < EnemyDataList.Count; i++)
            {
                if (EnemyDataList[i].SkinCode == SkinCode)
                    return EnemyDataList[i].SkeletonData;
            }
        }
        */

        return null;
    }

    public KDIE_ATTACK_TYPE GetSkin_AttackType(int _ID) { return GeneralsDataList[_ID - 1].Attack_type; }

    public string GetSkinIconName(int _ID) { if (_ID <= 0) _ID = 1;  return GeneralsDataList[_ID - 1].IconName; }
    public string GetSkinCode(int _ID) { if (_ID <= 0) _ID = 1;  return GeneralsDataList[_ID - 1].SkinCode; }
    public string GetWeaponCode(int _ID) { if (_ID <= 0) _ID = 1;  return GeneralsDataList[_ID - 1].WeaponCode; }
}