﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class SkillData
{
    public string IconName;                     //  아이콘 이름
    public string SkillName;
}

public class KDD_SkillData_Manager : MonoSingleton<KDD_SkillData_Manager>
{
    public List<SkillData> SkillDataList = new List<SkillData>();

    public override void Init()
    {
        DontDestroyOnLoad(this);
    }

    public string GetSkill_ResoureceName(int _ID) { return SkillDataList[_ID - 1].SkillName; }
    public string GetSkill_IconName(int _ID) { return SkillDataList[_ID - 1].IconName; }
}
