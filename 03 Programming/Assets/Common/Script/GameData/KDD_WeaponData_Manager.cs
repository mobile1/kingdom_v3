﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class WeaponData
{
    public KDIE_ATTACK_TYPE Attack_type;            //  무기의 공격 타입
    public string WeaponCode;                       //  무기 코드
    public List<int> MonuntSkill_ID = new List<int>();  //  장착된 스킬 ID 
}

public class KDD_WeaponData_Manager : MonoSingleton<KDD_WeaponData_Manager>
{
    public List<WeaponData> WeaponDataList = new List<WeaponData>();

    public override void Init()
    {
        DontDestroyOnLoad(this);
    }

    public KDIE_ATTACK_TYPE GetWeaponType(int _ID) { return WeaponDataList[_ID - 1].Attack_type; }
    public string GetWeaponCode(int _ID) { return WeaponDataList[_ID - 1].WeaponCode; }
    public List<int> GetWeaponMountSkills(int _ID) { return WeaponDataList[_ID - 1].MonuntSkill_ID; }

    public int GetWeaponID(string _WeaponCode)
    {
        for (int i = 0; i < WeaponDataList.Count; i++)
        {
            if (WeaponDataList[i].WeaponCode == _WeaponCode)
                return (i + 1);
        }

        return 0;
    }
	
}
