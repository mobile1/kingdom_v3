﻿using UnityEngine;
using System.Collections;

public enum KDIE_CHARACTER_TYPE
{
    GENERAL_TYPE = 1,   // 장군의 경우 하나의 애니메이션만 가지고 있음
    SOLDIER_TYPE,   // 병사 타입의 경우 여러 공격 형태를 애니메이션을 공통으로 가지고 있다.
    MAX
}

public enum KDIE_SKIN_TYPE
{
    basic = 1,  //  기본
    s2,     //  2성
    s3,     //  3성
    s4,     //  4성
    s5,     //  5성
    s6,     //  6성
    Max
}

public enum KDIE_ATTACK_TYPE
{
    SPEAR_TYPE = 1, // 창
    SWORD_TYPE,     // 검
    BOW_TYPE,       // 창
    THROW_TYPE,     // 활
    MACE_TYPE,      // 둔기
    FAN_TYPE,       // 부채 
    MAX
}
