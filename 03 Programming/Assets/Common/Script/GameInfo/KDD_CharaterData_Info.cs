﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System;
using Newtonsoft.Json;

public class CHARACTER_DATA
{
    public int id{get; set;}
    public int level{get; set;}
    public int attack{get; set;}
    public int defense{get; set;}
    public int avoidance{get; set;}
    public int hp{get; set;}
    public int mp{get; set;}
    public int move_speed{get; set;}
    public int attack_speed{get; set;}
    public int critical_chance{get; set;}
    public int hp_recovery{get; set;}
    public int mp_recovery{get; set;}
    public int power{get; set;}
    public int dexterity{get; set;}
    public int intelligence{get; set;}
    public int stamina{get; set;}
    public int stat_point{get; set;}
    public int experience{get; set;}
}

public struct CHARACTER_STATPOINT
{
	public int id{ get; set;}
	public int optionID_1{ get; set;}
	public double optionValue_1{ get; set;}
	public int optionID_2{ get; set;}
	public double optionValue_2{ get; set;}
}


public class KDD_CharaterData_Info : MonoSingleton<KDD_CharaterData_Info>{

    public List<CHARACTER_DATA> CharacterDataList;
	public List<CHARACTER_STATPOINT> CharacterStatPoint;

    public override void Init()
    {
        DontDestroyOnLoad(this);

        string textData = null;


        if (Application.platform == RuntimePlatform.WindowsPlayer)
        {
            string path = Application.dataPath;
            path = path.Substring(0, path.LastIndexOf('/'));
            path += "/test/GameInfoData/CharacterData/Character_Stat_Info.txt";

            if (File.Exists(path))
            {
                FileStream file = new FileStream(path, FileMode.Open, FileAccess.Read);
                StreamReader sr = new StreamReader(file);
                textData = sr.ReadToEnd();

                sr.Close();
                file.Close();
                Debug.Log("File Open Succeed");
            }
            else
                Debug.Log(path + " File Open Failed");
        }
        else
        {
            TextAsset textAsset = (TextAsset)Resources.Load("GameInfoData/CharacterData/Character_Stat_Info", typeof(TextAsset));
            textData = textAsset.text;

        }

        try
        {
            CharacterDataList = JsonConvert.DeserializeObject<List<CHARACTER_DATA>>(textData, new JsonSerializerSettings { TypeNameHandling = TypeNameHandling.All });
        }
        catch (Exception ex)
        {
            Debug.Log(ex.Message);
            throw;
        }

		TextAsset assetStatPoint = (TextAsset)Resources.Load("GameInfoData/CharacterData/Character_Stat_Point", typeof(TextAsset));
		CharacterStatPoint = JsonConvert.DeserializeObject<List<CHARACTER_STATPOINT>>(assetStatPoint.text, new JsonSerializerSettings { TypeNameHandling = TypeNameHandling.All });
    }

    /*
    public CHARACTER_DATA GetStoryEvent(int _id)
    {
        if (_id <= 0)
            _id = 1;

        if (_id < CharacterDataList.Count)
            return CharacterDataList[_id-1];

        Debug.Log("KDD_CharaterData_Info indx Overflow error");
        return null;
    }
    */
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
