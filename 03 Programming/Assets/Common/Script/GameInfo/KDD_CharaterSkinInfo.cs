﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System;
using Newtonsoft.Json;

public class CHARACTER_SKIN_DATA 
{
    public int ID{ get; set; }
    public string Name{ get; set; }
    public string ResourceName { get; set; }
    public int Skin_Level{ get; set; }
    public int Weapon_Type{ get; set; }
    public int General_01_ID{ get; set; }
    public int General_02_ID{ get; set; }
    public int General_03_ID{ get; set; }
    public int Rage_Maximum{ get; set; }
    public int Rage_Duration_Time{ get; set; }
    public int Option_01_ID{ get; set; }
    public int Option_01_Value_Min{ get; set; }
    public int Option_01_Value_Max{ get; set; }
    public int Option_02_ID{ get; set; }
    public int Option_02_Value_Min{ get; set; }
    public int Option_02_Value_Max{ get; set; }
    public int Option_03_ID{ get; set; }
    public int Option_03_Value_Min{ get; set; }
    public int Option_03_Value_Max{ get; set; }
    public string Option_01_Explain { get; set; }
    public string Option_02_Explain { get; set; }
    public string Option_03_Explain { get; set; }
}


public class KDD_CharaterSkinInfo : MonoSingleton<KDD_CharaterSkinInfo>
{
    public List<CHARACTER_SKIN_DATA> SkinDataList;

    public override void Init()
    {
        DontDestroyOnLoad(this);

        string textData = null;

        if (Application.platform == RuntimePlatform.WindowsPlayer)
        {
            string path = Application.dataPath;
            path = path.Substring(0, path.LastIndexOf('/'));
            path += "/test/GameInfoData/CharacterData/Character_Skin_Info.txt";

            if (File.Exists(path))
            {
                FileStream file = new FileStream(path, FileMode.Open, FileAccess.Read);
                StreamReader sr = new StreamReader(file);
                textData = sr.ReadToEnd();

                sr.Close();
                file.Close();
                Debug.Log("File Open Succeed");
            }
            else
                Debug.Log(path + " File Open Failed");
        }
        else
        {
            TextAsset textAsset = (TextAsset)Resources.Load("GameInfoData/CharacterData/Character_Skin_Info", typeof(TextAsset));
            textData = textAsset.text;
        }

        try
        {
            SkinDataList = JsonConvert.DeserializeObject<List<CHARACTER_SKIN_DATA>>(textData, new JsonSerializerSettings { TypeNameHandling = TypeNameHandling.All });
        }
        catch (Exception ex)
        {
            Debug.Log(ex.Message);
            throw;
        }
    }

    public CHARACTER_SKIN_DATA GetCharacterSkinData(int _id)
    {
        if (_id >= SkinDataList.Count)
            Debug.Log("KDD_CharaterSkinInfo indx Overflow error");

        if (_id <= 0)
            _id = 1;

        return SkinDataList[_id-1];
    }
}
