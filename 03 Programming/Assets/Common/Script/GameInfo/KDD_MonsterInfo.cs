﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System;
using Newtonsoft.Json;

public class MONSTER_DATA 
{
    public int ID{ get; set; }
	public string Name{ get; set; }
	public string ResourceName{ get; set; }
	public int Level{ get; set; }
    public int CharacterType{ get; set; }
	public int Attack_Type{ get; set; }
	public int Attack{ get; set; }
	public int Defense{ get; set; }
	public int Avoidance{ get; set; }
	public int HP{ get; set; }
	public int MP{ get; set; }
	public int Move_Speed{ get; set; }
	public int Attack_Speed{ get; set; }
	public int Attack_Range{ get; set; }
	public int Serach_Distance{ get; set; }
	public int Critical_Chance{ get; set; }
	public int HP_Recovery{ get; set; }
	public int MP_Recovery{ get; set; }
	public int Experience{ get; set; }
	public int General_Hit{ get; set; }
	public int Skill_Hit{ get; set; }
	public int Hit_Delay_Time{ get; set; }
	public int Skill_ID{ get; set; }
	public int AI_ID{ get; set; }
	public int Drop_ID{ get; set; }
    public int Rage_point { get; set; }
}


public class KDD_MonsterInfo : MonoSingleton<KDD_MonsterInfo>
{
    public List<MONSTER_DATA> MonsterDataList;

    public override void Init()
    {
        DontDestroyOnLoad(this);

        string textData = null;
        if (Application.platform == RuntimePlatform.WindowsPlayer)
        {
            string path = Application.dataPath;
            path = path.Substring(0, path.LastIndexOf('/'));
            path += "/test/GameInfoData/CharacterData/Monster_Info.txt";

            if (File.Exists(path))
            {
                FileStream file = new FileStream(path, FileMode.Open, FileAccess.Read);
                StreamReader sr = new StreamReader(file);
                textData = sr.ReadToEnd();

                sr.Close();
                file.Close();
                Debug.Log("File Open Succeed");
            }
            else
                Debug.Log(path + " File Open Failed");
        }
        else
        {
            TextAsset textAsset = (TextAsset)Resources.Load("GameInfoData/CharacterData/Monster_Info", typeof(TextAsset));
            textData = textAsset.text;
        }

        try
        {
            MonsterDataList = JsonConvert.DeserializeObject<List<MONSTER_DATA>>(textData, new JsonSerializerSettings { TypeNameHandling = TypeNameHandling.All });
        }
        catch (Exception ex)
        {
            Debug.Log(ex.Message);
            throw;
        }
    }


    public MONSTER_DATA GetMonsterData(int _id)
    {
        if (_id >= MonsterDataList.Count)
            Debug.Log("KDD_MonsterInfo indx Overflow error");

        if (_id <= 0)
            _id = 1;

        return MonsterDataList[_id-1];
    }

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
}
