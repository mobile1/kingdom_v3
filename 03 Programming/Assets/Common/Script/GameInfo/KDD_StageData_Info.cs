﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System;
using Newtonsoft.Json;

public class STAGE_DATA 
{
    public int Thema_ID{ get; set; }
    public string Thema_Name{ get; set; }                       
    public int Stage_ID{ get; set; }
    public string Stage_Name{ get; set; }
    public int Background_ID { get; set; }
    public int Field_Num{ get; set; }
    public int Limit_Time{ get; set; }
    public int Clear_Mode{ get; set; }
    public int Clear_Event_ID{ get; set; }
    public int Event_Monster_ID{ get; set; }
    public int Player_Pos{ get; set; }
    public int Ally_01_ID{ get; set; }
    public int Ally_01_Pos{ get; set; }
    public int Ally_02_ID{ get; set; }
    public int Ally_02_Pos{ get; set; }
    public int Defense_NPC_01_ID { get; set; }
    public int Defense_NPC_01_Pos { get; set; }
    public int Defense_NPC_02_ID { get; set; }
    public int Defense_NPC_02_Pos { get; set; }
    public int Defense_NPC_03_ID{ get; set; }
    public int Defense_NPC_03_Pos { get; set; }
    public int User_HP_Rate{ get; set; }
    public int User_MP_Rate{ get; set; }
    public int Monster_HP_Rate{ get; set; }
    public int HP_Recovery_Rate{ get; set; }
    public int MP_Recovery_Rate{ get; set; }
    public string Stage_Explain { get; set; }
}

public class KDD_StageData_Info : MonoSingleton<KDD_StageData_Info>
{
    public List<STAGE_DATA> StageDataList;

    public override void Init()
    {
        DontDestroyOnLoad(this);

        string textData = null;
        if (Application.platform == RuntimePlatform.WindowsPlayer)
        {
            string path = Application.dataPath;
            path = path.Substring(0, path.LastIndexOf('/'));
            path += "/test/GameInfoData/StageData/StageInfo.txt";

            if (File.Exists(path))
            {
                FileStream file = new FileStream(path, FileMode.Open, FileAccess.Read);
                StreamReader sr = new StreamReader(file);
                textData = sr.ReadToEnd();

                sr.Close();
                file.Close();
                Debug.Log("File Open Succeed");
            }
            else
                Debug.Log(path + " File Open Failed");
        }
        else
        {
            TextAsset textAsset = (TextAsset)Resources.Load("GameInfoData/StageData/StageInfo", typeof(TextAsset));
            textData = textAsset.text;
        }

        try
        {
            StageDataList = JsonConvert.DeserializeObject<List<STAGE_DATA>>(textData, new JsonSerializerSettings { TypeNameHandling = TypeNameHandling.All });
        }
        catch (Exception ex)
        {
            Debug.Log(ex.Message);
            throw;
        }
    }

    public STAGE_DATA GetStageData(int _Themaid, int _Stageid)
    {
        int nThemaID = _Themaid;
        int nStagetID = _Stageid;

        for (int i = 0; i < StageDataList.Count; i++)
        {
            if (StageDataList[i].Thema_ID == _Themaid && StageDataList[i].Stage_ID == _Stageid)
                return StageDataList[i];
        }

        Debug.Log("KDD_StageData_Info Can't found" + "ThemaID = " + _Themaid + "  StageID = " + _Stageid);
        return StageDataList[0];
    }
}
