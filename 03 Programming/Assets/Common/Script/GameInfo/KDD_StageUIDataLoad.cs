﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System;
using Newtonsoft.Json;

public class KDD_StageUIData
{
	public int themeID {get;set;}
	public string strTheme {get;set;}
	public List<StageDetail> detail {get;set;}
	
}

public class StageDetail
{
	public int level {get;set;}
	public List<StageMode> mode {get;set;}
	public string strExplain {get;set;}
	public string strTip {get;set;}
}

public class StageMode
{
	int nMode;
}


public class KDD_StageUIDataLoad : MonoSingleton<KDD_StageUIDataLoad>
{
	public List<KDD_StageUIData> listStageUIData;

	public override void Init()
	{
		TextAsset textAsset = (TextAsset)Resources.Load ("GameInfoData/StageData/StageUIData", typeof(TextAsset));
		try
		{
			listStageUIData = JsonConvert.DeserializeObject< List<KDD_StageUIData> >(textAsset.text, new JsonSerializerSettings { TypeNameHandling = TypeNameHandling.All });
		}
		catch(Exception ex)
		{
			Debug.Log(ex.Message);
			throw;
		}
	}

}