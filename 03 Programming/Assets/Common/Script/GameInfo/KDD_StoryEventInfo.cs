﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System;
using Newtonsoft.Json;

public class STORY_EVNET_DATA 
{
    public int Story_ID {get; set;}
    public int Start_Order{get; set;}
    public int NPC_ID{get; set;}
    public string Conversation{get; set;}
}

public class KDD_StoryEventInfo : MonoSingleton<KDD_StoryEventInfo>
{
    public List<STORY_EVNET_DATA> StoryEventList;

    void Awake()
    {
        DontDestroyOnLoad(this);
    }

    public override void Init()
    {
        string textData = null;
        if (Application.platform == RuntimePlatform.WindowsPlayer)
        {
            string path = Application.dataPath;
            path = path.Substring(0, path.LastIndexOf('/'));
            path += "/test/GameInfoData/Story_Event_Conversation.txt";
 
            if (File.Exists(path))
            {
                FileStream file = new FileStream(path, FileMode.Open, FileAccess.Read);
                StreamReader sr = new StreamReader(file);
                textData = sr.ReadToEnd();

                sr.Close();
                file.Close();
                Debug.Log("File Open Succeed");
            }
            else
                Debug.Log(path + " File Open Failed");
        }
        else
        {
            TextAsset textAsset = (TextAsset)Resources.Load("GameInfoData/Story_Event_Conversation", typeof(TextAsset));
            textData = textAsset.text;
        }

        try
        {
            StoryEventList = JsonConvert.DeserializeObject<List<STORY_EVNET_DATA>>(textData, new JsonSerializerSettings { TypeNameHandling = TypeNameHandling.All });
        }
        catch (Exception ex)
        {
            Debug.Log(ex.Message);
            throw;
        }
    }

    public STORY_EVNET_DATA GetStoryEvent(int _id)
    {
        if (_id < 1)
            return StoryEventList[0];

        if (_id > StoryEventList.Count)
        {
            Debug.Log("KDD_StoryEventInfo indx Overflow error");
            _id = StoryEventList.Count - 1;
        }

        return StoryEventList[_id-1];
    }

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
}
