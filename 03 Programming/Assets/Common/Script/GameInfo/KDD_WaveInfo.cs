﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System;
using Newtonsoft.Json;

public class WAVE_DATA 
{
    public int FIELD_ID{ get; set; }                        //  웨이브가 발생할 필드 ID
    public int WAVE_ID{ get; set; }                         //  웨이브 구분을 위한 ID
    public bool EVENT_ACTIVE_WAVE { get; set; }             //  이벤트로 동작하는 웨이브인지?
    public int EVENT_OCCURS_CONDITION{ get; set; }          //  이벤트 발생 조건 ID
    public int EVENT_STRORY_CONVERSATION_ID { get; set; }   //  이벤트 스토리 대화 ID
    public int EVENT_MOVE_POINT { get; set; }               //  몬스터 이동 지정 위치 ID
    public int EVENT_AVTIVE_MONSTER_HP_RATE { get; set; }   //  이벤트 발생 조건 중 몬스터의 HP 남은 비율 체크
    public int EVENT_AVTIVE_TIME { get; set; }              //  이벤트 발생 조건 중 몬스터 등장이후 일정 시간 이후 이벤트 발생
    public int START_TIME{ get; set; }                      //  이벤트 시작 시간 
    public int REPEAT_TIME{ get; set; }                     //  웨이브 반복 시간 간격
    public int WAVE_COUNT{ get; set; }                      //  반복할 웨이브 카운트
    public int MONSTER_ID{ get; set; }                      //  등장할 몬스터 ID
    public int START_POINT_ID{ get; set; }                  //  등장할 위치 ID
    public int MONSTER_AI_MODE { get; set; }                //  몬스터 AI 모드 ID
    public bool DEFEAT_MONSTER{ get; set; }                  //  격파 대상 몬스터인지?
    public int EVENT_ACTIVE_WAVE_ID01{ get; set; }          //  이벤트 조건이 되면 시작하게 할 웨이브 ID
    public int EVENT_ACTIVE_WAVE_ID02{ get; set; }
    public int EVENT_ACTIVE_WAVE_ID03{ get; set; }
    public int EVENT_ACTIVE_WAVE_ID04{ get; set; }
    public int EVENT_ACTIVE_WAVE_ID05{ get; set; }
    public int EVENT_ACTIVE_WAVE_ID06{ get; set; }
    public int EVENT_ACTIVE_WAVE_ID07{ get; set; }
    public int EVENT_ACTIVE_WAVE_ID08{ get; set; }
    public int EVENT_ACTIVE_WAVE_ID09{ get; set; }
    public int EVENT_ACTIVE_WAVE_ID10 { get; set; }
}


public class KDD_WaveInfo : MonoSingleton<KDD_WaveInfo> {

    public List<WAVE_DATA> WaveDataInfo;

    public override void Init()
    {

    }

    void OnDestory()
    {
        for(int i=0; i<WaveDataInfo.Count; i++)
            WaveDataInfo.Remove(WaveDataInfo[i]);

        WaveDataInfo.Clear();
    }

    public void LoadWave(string filename)
    {
        string textData = null;

        if (Application.platform == RuntimePlatform.WindowsPlayer)
        {
            string path = Application.dataPath;
            path = path.Substring(0, path.LastIndexOf('/'));
            string filestr = null;
            filestr += "/test/";
            filestr += filename;
            filestr += ".txt";

            path += filestr;

            if (File.Exists(path))
            {
                FileStream file = new FileStream(path, FileMode.Open, FileAccess.Read);
                StreamReader sr = new StreamReader(file);
                textData = sr.ReadToEnd();

                sr.Close();
                file.Close();
                Debug.Log("File Open Succeed");
            }
            else
                Debug.Log(path + " File Open Failed");
        }
        else
        {
            TextAsset textAsset = (TextAsset)Resources.Load(filename, typeof(TextAsset));
            textData = textAsset.text;
        }

        try
        {
            WaveDataInfo = JsonConvert.DeserializeObject<List<WAVE_DATA>>(textData, new JsonSerializerSettings { TypeNameHandling = TypeNameHandling.All });
        }
        catch (Exception ex)
        {
            Debug.Log(ex.Message);
            throw;
        }
    }

    public int GetWaveID_Num()
    {
        return WaveDataInfo.Count;
    }

    public WAVE_DATA GetWaveInfoData(int _id)
    {
        if (_id >= WaveDataInfo.Count)
            Debug.Log("KDD_StoryEventInfo indx Overflow error");

        return WaveDataInfo[_id];
    }
}
