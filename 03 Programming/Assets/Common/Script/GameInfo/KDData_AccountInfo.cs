﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System;
using Newtonsoft.Json;

using System.Runtime.Serialization.Formatters.Binary;

public class KDData_AccountInfo : MonoSingleton<KDData_AccountInfo> {
	
	[HideInInspector] public KDData_Account account;


	private int _level;
	public int level {
		get{
			return _level;
		}
		set{
			_level = SetLevel((int)value);
		}
	}

	public override void Init()
	{
		if(ReadBinaryFile())
		{
			level = account.exp;
		}
		else
		{
			TextAsset textAsset = (TextAsset)Resources.Load ("Account", typeof(TextAsset));
			try
			{
				account = JsonConvert.DeserializeObject<KDData_Account>(textAsset.text, new JsonSerializerSettings { TypeNameHandling = TypeNameHandling.All });
				level = account.exp;
			}
			catch(Exception ex)
			{
				Debug.Log(ex.Message);
				throw;
			}
		}
	}

	int SetLevel(int exp)
	{
		List<CHARACTER_DATA> listData = KDD_CharaterData_Info.i.CharacterDataList;

		int len = listData.Count;
		int level = 0;
		for(int i = 0; i < len; ++i)
		{
			if(exp < listData[i].experience)
			{
				level = listData[i].level - 1;
				break;
			}
		}
		return level <= 1 ? 1 : level;
	}

	public void Save()
	{
		var binaryFomatter  = new BinaryFormatter();

		string str = JsonConvert.SerializeObject (account);
#if UNITY_EDITOR
		var fileStream = File.Open (Application.dataPath + "/Common/Data/Account.json", FileMode.Create);
#else
		var fileStream = File.Open (Application.persistentDataPath + "/Common/Data/Account.json", FileMode.Create);
#endif

		binaryFomatter.Serialize(fileStream, str);
		fileStream.Close ();
	}

	public bool ReadBinaryFile()
	{
#if UNITY_EDITOR
		if(File.Exists(Application.dataPath + "/Common/Data/Account.json") == false)
			return false;
#else
		if(File.Exists(Application.persistentDataPath + "/Common/Data/Account.json") == false)
			return false;
#endif

		var binaryFomatter  = new BinaryFormatter();
#if UNITY_EDITOR
		var fileStream = File.Open (Application.dataPath + "/Common/Data/Account.json", FileMode.Open);
#else
		var fileStream = File.Open (Application.persistentDataPath + "/Common/Data/Account.json", FileMode.Open);
#endif
		string strValue = (string)binaryFomatter.Deserialize (fileStream);
		fileStream.Close ();
		account = JsonConvert.DeserializeObject<KDData_Account>(strValue, new JsonSerializerSettings { TypeNameHandling = TypeNameHandling.All });

		return true;
	}
}
