﻿
public class KDData_Character
{
	public string name { get; set;}
	public int index {get;set;}
	public int skill {get;set;}
	public string skillName {get;set;}
	public string skillExplain {get;set;}
	public int enchant { get; set; }
}
