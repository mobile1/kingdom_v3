﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System;
using Newtonsoft.Json;

using System.Runtime.Serialization.Formatters.Binary;

public class KDData_CharacterInfo : MonoSingleton<KDData_CharacterInfo> {
	
	public List<KDData_Character> characterList;

	public override void Init()
	{
		if(ReadBinaryFile() == false)
		{
			TextAsset textAsset = (TextAsset)Resources.Load ("CharInfo", typeof(TextAsset));
			try
			{
				characterList = JsonConvert.DeserializeObject<List<KDData_Character>>(textAsset.text, new JsonSerializerSettings { TypeNameHandling = TypeNameHandling.All });
			}
			catch(Exception ex)
			{
				Debug.Log(ex.Message);
				throw;
			}
		}

	}

	public void Save()
	{
		var binaryFomatter  = new BinaryFormatter();
		
		string str = JsonConvert.SerializeObject (characterList);
		#if UNITY_EDITOR
		var fileStream = File.Open (Application.dataPath + "/Common/Data/CharInfo.json", FileMode.Create);
		#else
		var fileStream = File.Open (Application.persistentDataPath + "/Common/Data/CharInfo.json", FileMode.Create);
		#endif
		
		binaryFomatter.Serialize(fileStream, str);
		fileStream.Close ();
	}
	
	public bool ReadBinaryFile()
	{
		#if UNITY_EDITOR
		if(File.Exists(Application.dataPath + "/Common/Data/CharInfo.json") == false)
			return false;
		#else
		if(File.Exists(Application.persistentDataPath + "/Common/Data/CharInfo.json") == false)
			return false;
		#endif
		
		var binaryFomatter  = new BinaryFormatter();
		#if UNITY_EDITOR
		var fileStream = File.Open (Application.dataPath + "/Common/Data/CharInfo.json", FileMode.Open);
		#else
		var fileStream = File.Open (Application.persistentDataPath + "/Common/Data/CharInfo.json", FileMode.Open);
		#endif
		string strValue = (string)binaryFomatter.Deserialize (fileStream);
		fileStream.Close ();
		characterList = JsonConvert.DeserializeObject<List<KDData_Character>>(strValue, new JsonSerializerSettings { TypeNameHandling = TypeNameHandling.All });
		
		return true;
	}
}
