﻿using UnityEngine;
using System.Collections;

public class KDData_GameInfo : MonoSingleton<KDData_GameInfo>
{

    public struct sStage
    {
        public int level;
        public int detailLevel;
    };

    [HideInInspector]
    static int charIndex;
	static int WeaponIndex;
    static sStage stage;

    public void SetGameData(int _charIndex, int _WeaponIndex, int _level, int _detailLevel)
    {
        charIndex = _charIndex;
        WeaponIndex = _WeaponIndex;
        stage.level = _level;
        stage.detailLevel = _detailLevel;
    }

    public override void Init()
    {
        DontDestroyOnLoad(this);
    }

    public int GetWeaponIndex() { return WeaponIndex; }
    public int GetCharterIndex() { return charIndex; }
    public sStage GetStageInfo() { return stage; }
}