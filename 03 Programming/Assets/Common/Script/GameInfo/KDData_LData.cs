﻿using UnityEngine;
using System.Collections;

public static class KDData_LData {

	public static bool setting_bg;
	public static bool setting_Eff;
	public static bool setting_Vibrate;
	public static bool setting_Push;

	public static void SaveSetting(bool bBg, bool bEff, bool bVibrate, bool bPush)
	{
		setting_bg = bBg;
		setting_Eff = bEff;
		setting_Vibrate = bVibrate;
		setting_Push = bPush;

		PlayerPrefs.SetInt ("BG", bBg ? 1 : 0);
		PlayerPrefs.SetInt ("EFF", bEff ? 1 : 0);
		PlayerPrefs.SetInt ("VIBRATE", bVibrate ? 1 : 0);
		PlayerPrefs.SetInt ("PUSH", bPush ? 1 : 0);
	}

	public static void Init()
	{
		bool bBg = ( PlayerPrefs.GetInt ("BG", 1) == 1) ? true : false;
		bool bEff = ( PlayerPrefs.GetInt ("EFF", 1) == 1) ? true : false;
		bool bVibrate = ( PlayerPrefs.GetInt ("VIBRATE", 1) == 1) ? true : false;
		bool bPush = ( PlayerPrefs.GetInt ("PUSH", 1) == 1) ? true : false;
		
		setting_bg = bBg;
		setting_Eff = bEff;
		setting_Vibrate = bVibrate;
		setting_Push = bPush;
	}

/*	public static void GetSetting(out bool bBg, out bool bEff, out bool bPush)
	{

		int bgValue = PlayerPrefs.GetInt ("BG", 1);
		int effValue = PlayerPrefs.GetInt ("EFF", 1);
		int pushValue = PlayerPrefs.GetInt ("PUSH", 1);
		bBg = (bgValue == 1) ? true : false;
		bEff = (effValue == 1) ? true : false;
		bPush = (pushValue == 1) ? true : false;

		setting_bg = bBg;
		setting_Eff = bEff;
		setting_Push = bPush;
	}
	*/
}
