﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System;
using Newtonsoft.Json;

using System.Runtime.Serialization.Formatters.Binary;

public class KDData_StageInfo : MonoSingleton<KDData_StageInfo> {

	public List<KDData_Stage> stageList;

	void Awake()
	{
		if(ReadBinaryFile() == false)
		{
			TextAsset textAsset = (TextAsset)Resources.Load ("StageInfo", typeof(TextAsset));
			
			try{
				stageList = JsonConvert.DeserializeObject<List<KDData_Stage>>(textAsset.text, new JsonSerializerSettings { TypeNameHandling = TypeNameHandling.All });
			}
			catch(Exception ex)
			{
				Debug.Log(ex.Message);
				throw;
			}
		}
	}

	public void Save()
	{
		var binaryFomatter  = new BinaryFormatter();
		
		string str = JsonConvert.SerializeObject (stageList);
		#if UNITY_EDITOR
		var fileStream = File.Open (Application.dataPath + "/Common/Data/StageInfo.json", FileMode.Create);
		#else
		var fileStream = File.Open (Application.persistentDataPath + "/Common/Data/StageInfo.json", FileMode.Create);
		#endif
		
		binaryFomatter.Serialize(fileStream, str);
		fileStream.Close ();
	}
	
	public bool ReadBinaryFile()
	{
		#if UNITY_EDITOR
		if(File.Exists(Application.dataPath + "/Common/Data/StageInfo.json") == false)
			return false;
		#else
		if(File.Exists(Application.persistentDataPath + "/Common/Data/StageInfo.json") == false)
			return false;
		#endif
		
		var binaryFomatter  = new BinaryFormatter();
		#if UNITY_EDITOR
		var fileStream = File.Open (Application.dataPath + "/Common/Data/StageInfo.json", FileMode.Open);
		#else
		var fileStream = File.Open (Application.persistentDataPath + "/Common/Data/StageInfo.json", FileMode.Open);
		#endif
		string strValue = (string)binaryFomatter.Deserialize (fileStream);
		fileStream.Close ();
		stageList = JsonConvert.DeserializeObject<List<KDData_Stage>>(strValue, new JsonSerializerSettings { TypeNameHandling = TypeNameHandling.All });
		
		return true;
	}
}
