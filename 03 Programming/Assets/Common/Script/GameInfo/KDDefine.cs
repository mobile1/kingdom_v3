﻿public class KDDefine
{
	public enum eSTATEPOINT{POWER, SPEED, DEX, DEF};
	public enum ePOPUPTYPE {NONE, SKININFO, POPUPOK, ITEMINFO, ITEM_SKILLINFO, ITEM_SKILLCHANGE, POPUPNOTICE};
	public enum eBGTYPE{BASE, ENCHANT};
	public enum eOKPOPUPTYPE{SKILLCHANGE, NOTICE};

	public struct sSkillInfo
	{
		public int index;
		public string strName;
		public string strExplain;

	}

	public static int[] InvenItemRowPos = {-190,  - 95,  1, 96, 191};
	public static int[] InvenItemColPos = {132, 34, -64, -162};

	public static void GetInvenPos(int _index, out int out_X, out int out_Y)
	{
		if(_index == 0)
		{
			out_X = InvenItemRowPos[0];
			out_Y = InvenItemColPos [0];
		}
		else
		{
			int index_X = _index % 5;
			out_X = InvenItemRowPos[index_X];
			int index_Y = _index / 5;
			out_Y = InvenItemColPos [index_Y];
		}
	}

	public enum eStatOption
	{
		ATK = 1,		//공격력(정수)
		ATK_PER,		//공격력%
		DEF,			//방어력
		AGI,			//회피율
		MAX_HP,			//최대 HP 량
		MAX_MP,			//최대 MP 량
		MOVESPEED_PER,	//이동속도 %
		ATKSPEED_PER,	//공격속도 %
		CRI_PER,		//치명타 확률 %
		HP_RECOVERY,	//HP회복 %
		MP_RECOVERY,	//Mp회복 %
		POWER,			//힘
		DEX,			//민첩
		INT,			//지능
		HP,				//체력
		SKILL,			//스킬ID
		EXP_PER,		//경험치증가 %(결과 화면 표시되는 최종 경험치 에서 입력한 %만큼 추가로 지금
		ANGRY_TIME,		//분노 유지시간
		GET_RANGE		//획득 사정거리
	};
}