﻿using UnityEngine;
using System.Collections;

//  빈 게임 오브젝트에 하위 오브젝트를 삭제하지 않기 위해서 작성

public class GameObjectDontDetory : MonoBehaviour
{
    void Awake()
    {
        DontDestroyOnLoad(this);
    }
	
}
