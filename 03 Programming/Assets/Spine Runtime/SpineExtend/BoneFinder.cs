﻿using UnityEngine;
using System.Collections;
using Spine;

[ExecuteInEditMode]
public class BoneFinder : MonoBehaviour {

	public SkeletonAnimation skAnimation;
	public int boneNameIdx;
	public Spine.Bone bone;

	// Use this for initialization
	void Start () {
		if (skAnimation != null)
			bone = skAnimation.skeleton.Bones [boneNameIdx];
	}
	
	// Update is called once per frame
	void Update () {
		if (bone != null) {
			transform.localPosition = new Vector3 (bone.WorldX, bone.WorldY, transform.localPosition.z);
			transform.localRotation = Quaternion.Euler (0, 0, bone.WorldRotation - 90);
			transform.localScale = new Vector3 (bone.WorldScaleX, bone.WorldScaleY, transform.localScale.z);
		}
	}
}
