﻿

using UnityEngine;
using UnityEditor;
using UnityEditorInternal;

using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
//using GMT;

[ExecuteInEditMode]
[CustomEditor(typeof(BoneFinder))]
public class BoneFinederEditor: Editor {

	BoneFinder finder;
	bool showDefaultVariablies = false;

	public List<string> actionTypeList = new List<string>();
	public int actionTypeIdx;
	public List<string> actionNameList = new List<string>();
	public int actionNameIdx;

	public List<string> boneNameList = new List<string>();

	bool isPlay;

	void OnEnable(){

		finder = (BoneFinder)target;

		showDefaultVariablies = Boolean.Parse( PlayerPrefs.GetString("BoneFinder","false") );

		if (finder.skAnimation == null)
			finder.skAnimation = finder.transform.parent.GetComponent<SkeletonAnimation> ();
	}
	
	void OnDisable(){
	}
	
	public override void OnInspectorGUI(){	
		
		if(! finder.gameObject.activeInHierarchy ) return;

		finder.skAnimation = EditorGUILayout.ObjectField("SK Animation Comp" , finder.skAnimation, typeof( SkeletonAnimation ), true) as SkeletonAnimation;
		
		if (finder.skAnimation != null && finder.skAnimation.skeleton != null) {
			// Initial skin name.
			string[] skins = new string[finder.skAnimation.skeleton.Data.Skins.Count];
			int skinIndex = 0;
			for (int i = 0; i < skins.Length; i++) {
				string name = finder.skAnimation.skeleton.Data.Skins[i].Name;
				skins[i] = name;
				if (name == finder.skAnimation.initialSkinName)
					skinIndex = i;
			}
			
			EditorGUILayout.BeginHorizontal();
			EditorGUILayout.LabelField("Initial Skin & Clip");
			EditorGUIUtility.LookLikeControls();
			skinIndex = EditorGUILayout.Popup(skinIndex, skins);
			
			
			finder.skAnimation.initialSkinName = skins[skinIndex];
			
			// Animation name.
			string[] animations = new string[finder.skAnimation.skeleton.Data.Animations.Count + 1];
			animations[0] = "<None>";
			int animationIndex = 0;
			for (int i = 0; i < animations.Length - 1; i++) {
				string name = finder.skAnimation.skeleton.Data.Animations[i].Name;
				animations[i + 1] = name;
				if (name == finder.skAnimation.AnimationName)
					animationIndex = i + 1;
			}
			
			
			EditorGUIUtility.LookLikeControls();
			int temp = animationIndex;
			
			animationIndex = EditorGUILayout.Popup(animationIndex, animations);
			
			if( animationIndex != temp ){
				if (animationIndex == 0)
					finder.skAnimation.AnimationName = null;
				else
					finder.skAnimation.AnimationName = animations[animationIndex];
			}
			
			EditorGUILayout.EndHorizontal();

			int bLen = finder.skAnimation.skeleton.Bones.Count;
			string[] boneNames = new string[bLen];
			for( int i = 0; i < bLen; i++){
				string name = finder.skAnimation.skeleton.Bones[i].Data.Name;
				boneNames[i] = name;
			}
			finder.boneNameIdx = EditorGUILayout.Popup( finder.boneNameIdx, boneNames );

			finder.bone = finder.skAnimation.skeleton.Bones[finder.boneNameIdx];
//			if( !isPlay && GUILayout.Button("Play") ){
//				isPlay = true;
//			}else if( isPlay && GUILayout.Button("Stop") ){
//				isPlay = false;
//			}
		}
	
			
		
		{
			EditorGUILayout.Separator();
		}
		
		//update and redraw:
		if(GUI.changed){
			EditorUtility.SetDirty(finder);	
			finder.name = finder.bone.Data.Name;
			finder.skAnimation.Update();
		}
	}
}
