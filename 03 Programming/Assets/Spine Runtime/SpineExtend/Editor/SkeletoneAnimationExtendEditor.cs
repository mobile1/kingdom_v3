﻿using UnityEditor;
using UnityEngine;


[CustomEditor(typeof(SkeletonAnimationExtend))]
public class SkeletoneAnimationExtendEditor : SkeletonAnimationInspector {
	bool defaultFold = true;
	bool extendFold = true;

	SkeletonAnimationExtend component;

	protected override void OnEnable () {
		component = (SkeletonAnimationExtend)target;
		base.OnEnable ();
	}

	void OnDisable(){
		component = null;
	}

	protected override void gui () {
		if( component.skeletonDataAsset == null ){
			component.skeletonDataAsset = EditorGUILayout.ObjectField("Skeletone Data Asset", component.skeletonDataAsset,typeof( SkeletonDataAsset ),true) as SkeletonDataAsset;

		}else{

			if (defaultFold = EditorGUILayout.Foldout (defaultFold,"Default Skeletone Variable")) {
				EditorGUI.indentLevel += 1;
				base.gui ();
				EditorGUI.indentLevel -= 1;
			}

			if (extendFold = EditorGUILayout.Foldout (extendFold, "Extend Variable")) {
				EditorGUI.indentLevel += 1;
				ExtendInspector();
				EditorGUI.indentLevel -= 1;
			}
		}
	}

	bool visibleWeaponBoundings;

	void ExtendInspector(){

		if( component.skeletonDataAsset == null ) return;

		visibleWeaponBoundings = EditorGUILayout.Foldout(visibleWeaponBoundings ,"Bone To Synchers");

		if( visibleWeaponBoundings ){
			EditorGUI.indentLevel += 1;
			int len = component.weaponBoundingBoxs.Count;
			for( int i = 0; i < len ; i++){
				EditorGUILayout.ObjectField("BoneBoundingBox" + i, component.weaponBoundingBoxs[i],typeof(BoneBoundingBox),true);
			}
			EditorGUI.indentLevel -= 1;
		}

		if ( component.bodyBoundingBox != null ) EditorGUILayout.ObjectField("BodyBounding", component.bodyBoundingBox,typeof(BoneBoundingBox),true);

		EditorGUILayout.BeginHorizontal();
		{
			if (GUILayout.Button ("Create Weapon Bound ")) 
			{
				component.CreateWeaponBoundingBox();
	//			MoveObject( go );
			}

			if (GUILayout.Button ("Delete Weapon Bound ")) 
			{
				component.DeleteWeaponBoundingBox( component.weaponBoundingBoxs.Count - 1 );
			}
		}
		EditorGUILayout.EndHorizontal();

		EditorGUILayout.BeginHorizontal();
		{
			if (GUILayout.Button ("Create Body Bound ")) 
			{
				component.CreateBodyBoundingBox();
			}
			if (GUILayout.Button ("Delete Body Bound ")) 
			{
				component.DeleteBodyBoundingBox();
			}
		}
		EditorGUILayout.EndHorizontal();


		if (GUILayout.Button ("Add GSorting Layer Component")) {
			if( component.gameObject.GetComponent<GSortingLayer>() == null ){
				component.gameObject.AddComponent<GSortingLayer>();
			}
		}
	}

	// Scene view 에서 해당 obejct 로 포커싱을 해주는 함수
	void MoveObject( GameObject go){			
		UnityEditor.Selection.activeObject = go;
		try {
			UnityEditor.SceneView.lastActiveSceneView.FrameSelected();	
		} catch (System.Exception ex) {
			Debug.Log( "move object exception is passed " + ex);
		}
	}
}
