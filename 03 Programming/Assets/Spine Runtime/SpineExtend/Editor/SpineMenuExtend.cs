﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using UnityEngine;

public class SpineMenuExtend : MonoBehaviour {

	[MenuItem("GameObject/Create Other/Spine SkeletonAnimation Extend")]
	static public void CreateSkeletonAnimationExtendGameObject () {
		GameObject gameObject = new GameObject("New SkeletonAnimation Extend", typeof(SkeletonAnimationExtend));
		EditorUtility.FocusProjectWindow();
		Selection.activeObject = gameObject;
	}
}
