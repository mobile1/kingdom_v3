﻿/* 
	테스트용 캐릭터 클래스

	*/


using UnityEngine;
using System.Collections;
using System;

using skMG = SkeletonDataAssetManager;

public class SpineExtendCharacter : MonoBehaviour {
	// 애니메이션 클립.
	public enum ClipType{atk,down,hit_01,hit_02,idle,run};

    public KDIE_CHARACTER_TYPE characterType = KDIE_CHARACTER_TYPE.GENERAL_TYPE;
    public KDIE_ATTACK_TYPE group = KDIE_ATTACK_TYPE.SPEAR_TYPE;

	public int 	weaponGrade = 1;

	public ClipType 		currentClipType;

	public string 			characterCode = "GAS001";

	public string 			weaponCode = "GAW001";

	public string 			bulletCode = "GCB001";

	// 움짐임 벡터
	public Vector3 moveVector = Vector3.zero;
	public float moveSpeed = 1000;
	Transform mytf;

	// 스파인 애니메이션.
	public SkeletonAnimationExtend skAnimation;
//	public SkeletonAnimation skAnimation;

	// 공격등의 한번 실행 되고 마는 애니며이션 체크용 함수.
	public bool isOneShotAnimationning;

	// 테스트용 총알을 만드는 클래스.
	public SpineExtendCharacterBulletMaker bulletMaker;

	// 자동으로 Start함수에서 초기화 할 것인가.
	// 하드코딩으로 본 클래스를 생성 하는 경우 초기화 작업을 별도로 해줘야 하기에 다음 변수를 두어 중복처리를 막는다. 
	// 하이라키에 등록 해서 쓰는 경우에는 true, 소스상에서 생성 하는 경우에는 false.
	public bool autoInitSkeleton = false;

	// 본 캐릭터가 플레이어 캐릭터 인가.
	public bool isPlayer = false;

	public bool useBullet;

	// Use this for initialization
	void Start () {
		mytf = transform;
		// 자동 초기화 할경우 .
		if( autoInitSkeleton ) InitSkeletonAnimationExtend( characterCode, weaponGrade, weaponCode);

		// 플레이어 캐릭터 이면 알파값을 빼준다.
		// SkeletoneAnimationExtend 클래스에서는 컬러를 조절 할수 있는 함수가 포함되어 있다.
//		if( isPlayer ) skAnimation.ActionFadeTo(3f,0.5f);
		// 몸체 바디의 충돌영역을 2배로 키워준다.
		// 스파인상의 바다 이미지 크기만으로는 영역이 작아 소스상에서 일괄적으로 키워준다.
	}

	// Update is called once per frame
	void Update () {
		// 키보드 컨트롤.
		if( isPlayer ) ActionProcess ();
	}

	void OnGUI(){

		string str = "";
		str += "Z : 장군 & 병사 => " + characterType + "\n";
		str += "X : 캐릭터 병과 그룹 변경 => " + group + "\n";
		str += "C : 캐릭터 코드 변경 => " + characterCode + "\n";
		str += "V : 무기 코드 변경 => " + weaponCode + "\n";
		str += "B : 무기 강화 수치 변경 => " + weaponGrade + "\n";
		str += "N : 총알 변경 => " + bulletCode + "\n";
		str += "N : 총알 강화 변경 => " + bulletCode + "\n";
		str += "방향키 : 이동\n";
		str += "스페이스 : 공격\n";

		GUI.Label(new Rect(0,0,1000,1000), str );
	}
	// 키보드 컨트롤.
	void ActionProcess(){
		moveVector = Vector3.zero;
		// 키보드에 의한 움직임.
		if( ! isOneShotAnimationning ) moveVector = GetMoveVectFromInputKey().normalized;
		// 단축키 처리.
		CheckInputKey();
		// 움직임 벡터에 따라 애니메이션 상태를 변경 시켜 준다.
		AutoSetAniState( moveVector.magnitude );

		// Flip
		if( moveVector.x > 0 && mytf.eulerAngles.y != 0 ) 
			mytf.eulerAngles = new Vector3(0,0,0);
		else if( moveVector.x < 0 && mytf.eulerAngles.y != 180 ) 
			mytf.eulerAngles = new Vector3(0,180,0);


		//**start move by translate
		Vector3 myPos = mytf.position;
		myPos += moveVector * (moveSpeed * Time.smoothDeltaTime);

//		Rect rect = KDI_MapTool.i.characterMoveRect;
//		if (myPos.x < rect.xMin || myPos.x > rect.xMax)	myPos.x = mytf.position.x;
//		if (myPos.y < rect.yMin || myPos.y > rect.yMax)	myPos.y = mytf.position.y;

		mytf.position = myPos;
		// **end move by translate */
	}
	// 키보드 컨트롤.
	Vector3 GetMoveVectFromInputKey(){
		return new Vector3( Input.GetAxis ("Horizontal"),Input.GetAxis ("Vertical"),0f);
	}
	// 단축키.
	// 병과 변경, 스킨 변경.
	int characterIdx = 0;
	char characterGroupName = 'A';
	int weaponIdx = 0;

	void CheckInputKey(){


		// 공격 버튼.
		if( Input.GetKeyUp( KeyCode.Space )){
			ActionAttack();
		}
		// general or soldier
		else if( Input.GetKeyUp( KeyCode.Z ) ){

            characterType = characterType == KDIE_CHARACTER_TYPE.GENERAL_TYPE ? KDIE_CHARACTER_TYPE.SOLDIER_TYPE : KDIE_CHARACTER_TYPE.GENERAL_TYPE;

			characterIdx = 0;

			weaponIdx = 0;

			group = KDIE_ATTACK_TYPE.SWORD_TYPE;

			characterCode = skMG.i.GetCharacterSKData(characterType, group)[characterIdx].name;

			weaponCode = skMG.i.GetWeaponCode( characterType, group )[weaponIdx];

			InitSkeletonAnimationExtend();
		}
		// character code change =>> Group
		else if( Input.GetKeyUp( KeyCode.X ) )
		{
			characterIdx = 0;
			
			weaponIdx = 0;

            int i = (int)group >= (int)KDIE_ATTACK_TYPE.MAX ? 0 : (int)group + 1;

            group = (KDIE_ATTACK_TYPE)i;//Enum.Parse( typeof( skMG ), i );

			// Code Setting
			characterCode = skMG.i.GetCharacterSKData( characterType, group )[characterIdx].name;
		
			weaponCode = skMG.i.GetWeaponCode( characterType, group )[weaponIdx];

			weaponGrade = 1;

			InitSkeletonAnimationExtend();
		}
		// character code change = > number
		else if( Input.GetKeyUp( KeyCode.C ) )
		{
			characterIdx = characterIdx + 1 >= skMG.i.GetCharacterSKData( characterType, group ).Count ? 0 : characterIdx + 1;

			weaponIdx = 0;

			//Code Setting
			characterCode = skMG.i.GetCharacterSKData( characterType, group )[characterIdx].name;

			weaponCode = skMG.i.GetWeaponCode( characterType, group )[weaponIdx];

			weaponGrade = 1;

			InitSkeletonAnimationExtend();
		}
		//weapon code change
		else if( Input.GetKeyUp( KeyCode.V ) )
		{
			weaponIdx = weaponIdx + 1 >= skMG.i.GetWeaponCode( characterType, group ).Length ? 0 : weaponIdx + 1;

			weaponCode = skMG.i.GetWeaponCode( characterType, group )[weaponIdx];

			InitSkeletonAnimationExtend();
		}
		else if( Input.GetKeyUp( KeyCode.B ) )
		{
			weaponIdx = 0;

			weaponGrade = weaponGrade + 1 > 3 ? 1 : weaponGrade + 1;

			InitSkeletonAnimationExtend();
		}
	}


	// 스켈레톤 애니메이션 클래스를 초기화 한다.
	public void InitSkeletonAnimationExtend( ){
		InitSkeletonAnimationExtend(characterCode, weaponGrade, weaponCode);
	}
	// 스켈레톤 애니메이션 클래스를 초기화 한다.
	// 바운딩 영역을 결정 하는 본과 이미지 네임을 하드코딩으로 자동 처리 해주는 부분.
	public void InitSkeletonAnimationExtend(string _characterCode, int _weaponGrade, string _weaponCode ){

		skAnimation.state.End -= AnimationEnd;

		characterCode = _characterCode;
		weaponCode = _weaponCode;
		weaponGrade = _weaponGrade;

		skAnimation.SetSkeletonDataAsset( skMG.i.GetCharacterSKData(characterType, group)[characterIdx] );

		// 장수의 경우 스킨 변경.
		if( characterType == KDIE_CHARACTER_TYPE.GENERAL_TYPE )
		{
			ChangeWeapon( _weaponGrade, _weaponCode );
		}
		// 병사 일경우 공격 타입 변경.
        else if (characterType == KDIE_CHARACTER_TYPE.SOLDIER_TYPE)
        {
//			ChangeAttackType( _weaponCode );
		}

		// 몸체 충돌 영역 설정.
		if( skAnimation.bodyBoundingBox != null ){
			skAnimation.bodyBoundingBox.InitBoundingBox("Body", characterCode + "_" + "Body", true);
		}

		skAnimation.skeleton.SetSkin("basic");

//		skAnimation.AnimationName = SpineExtendCharacter.ClipType.idle.ToString();

//		skAnimation.skeletonDataAsset.Reset();
//		skAnimation.Reset();

		// 최종 설정후 반드시 애니메이션 클립을 하나 호출 해줘야 한다. 
		SetClip( SpineExtendCharacter.ClipType.idle );

		// 애니메이션이 끝남을 알기위한 이벤트 등록./
		skAnimation.state.End += AnimationEnd;
	}

	// 스킨 변경.
	// 장수용 함수로써 무기 본의 이름과 해당 이미지 네임을 가지고 영역을 잡아주는 역활을 한다.
	// 하드 코딩 소스.
	// 원칙 적으로는 BoneBoundingBox의 변수를 조절 하여 잡는 것이 맞으나 소스상에서 처리를 해준다.
	public void ChangeWeapon(int _weaponGrade, string _weaponCode){
		weaponGrade = _weaponGrade;
		weaponCode = _weaponCode;

		skAnimation.skeleton.SetSlotsToSetupPose();

		useBullet = false;

		if( skAnimation.skeleton.FindSlot("Weapon") != null )
		{
			try{
//				skAnimation.skeleton.SetAttachment("Weapon", "Weapons/" + weaponCode + "/" + weaponCode + "_0" + weaponGrade);
				skAnimation.skeleton.SetAttachment("Weapon", "Weapons/" + weaponCode + "/" + weaponCode + "_0" + 1);
//				skAnimation.weaponBoundingBoxs[0].InitBoundingBox( "BoundingBox",weaponCode + "_0" + weaponGrade); 
				skAnimation.weaponBoundingBoxs[0].InitBoundingBox( "BoundingBox","BoundingBox", true); 
			}catch(Exception e){
				print ( "Not Found Sprite Name : " + e.ToString() );
			}
		}
		// multi parts ex) bow
		else if( skAnimation.skeleton.FindSlot("Weapon_A") != null )
		{
			try{
				bool isBe = false;

//				string path = "Weapons/" + weaponCode + "/" + weaponCode + "_0" + weaponGrade + "/" + weaponCode + "_0" + weaponGrade + "_";
				string path = "Weapons/" + weaponCode + "/" + weaponCode + "_0" + 1 + "/" + weaponCode + "_0" + 1 + "_";

				char startChar = 'A';

				int startCharToInt = Convert.ToInt16( startChar );
				if( skAnimation.skeleton.GetAttachment("Weapon_" + startChar,path + startChar) != null ) isBe = true;
//				print ( "Weapon_" + startChar + " " + path + startChar );
				int i = 0; 

				while( isBe ){
					char addCode = Convert.ToChar( startCharToInt + i);

					skAnimation.skeleton.SetAttachment("Weapon_" + addCode, path + addCode);

					addCode = Convert.ToChar( startCharToInt + (++i) );

					if( skAnimation.skeleton.GetAttachment("Weapon_" + addCode, path + addCode ) == null ) isBe = false;
				}

				skAnimation.weaponBoundingBoxs[0].InitBoundingBox( "BoundingBox","BoundingBox", false); 

				if( skAnimation.skeleton.FindSlot("Bullet") != null ) useBullet = true;

			}catch(Exception e){
				print ( "Not Found Sprite Name : " + e.ToString() );
			}
		}
		// only Bullet
		else if( skAnimation.skeleton.FindSlot("Bullet") != null )
		{
			try{
				bulletCode = weaponCode;
//				skAnimation.skeleton.SetAttachment("Bullet","Bullets/" + weaponCode + "_0" + weaponGrade);
				skAnimation.skeleton.SetAttachment("Bullet","Bullets/" + weaponCode + "_0" + 1);
				skAnimation.weaponBoundingBoxs[0].InitBoundingBox( "BoundingBox","BoundingBox", true); 

				useBullet = true;

			}catch(Exception e){
				print ( "Not Found Sprite Name : " + e.ToString() );
			}
		}
	}

	// you must modify to name with SkeletoneAnimationExtend
//	public void ChangeAttackType( AttackType aType ){
//		attackType = aType;
//		SetClip( ClipType.idle );
//
//		switch( aType ){
//		case AttackType.a: skAnimation.weaponBoundingBoxs[0].InitBoundingBox( "Weapon_lance2"	,"LC001"); break;
//		case AttackType.b: skAnimation.weaponBoundingBoxs[0].InitBoundingBox( "Weapon_sword2"	,"SW001"); break;
//		case AttackType.c: skAnimation.weaponBoundingBoxs[0].InitBoundingBox( "Weapon_Arrow2"	,"AR001"); break;
//		case AttackType.d: skAnimation.weaponBoundingBoxs[0].InitBoundingBox( "Weapon_stone2"	,"ST001"); break;
//		case AttackType.e: skAnimation.weaponBoundingBoxs[0].InitBoundingBox( "Weapon_Hammer2"	,"HM001"); break;
//		}
//	}
	// 공격 명령.
	void ActionAttack(){
		SetClip(ClipType.atk);

		// 원거리 병과의 경우 총알을 발사 시킨다. 
		if( bulletMaker != null && skAnimation.weaponBoundingBoxs.Count > 0){
			if( useBullet ){
				bulletMaker.ActionBulletMaking(skAnimation.weaponBoundingBoxs[0]);
			}
		}
	}


	// 애니메이션이 1회 끝나면 호출. 
	void AnimationEnd (Spine.AnimationState state, int trackIndex){
		if( state.ToString() != ClipType.idle.ToString() && state.ToString() != ClipType.run.ToString() ) 
		{
//			print ("event end : " + state.ToString() );
			isOneShotAnimationning = false;
		}
	}

	// SetAnimation 간편 함수.
	void SetClip( string name ){
		SetClip( (ClipType) Enum.Parse( typeof(ClipType), name ) );
	}
	// SetAnimation 간편 함수.
	// 현재 1회성 애니 메이션인지. idle과 같은 반복형 애니가 구현중인지 알기 위한 처리 구문.
	void SetClip( ClipType cType){
		currentClipType = cType;

		if( currentClipType == ClipType.idle || currentClipType == ClipType.run )
		{
			SetAnimation( cType , true);
		}
		else
		{
			isOneShotAnimationning = true;
			SetAnimation( cType , false);
		}
	}
	// 병과별, 장수별 애니 클립 네이밍이 달라 파싱해주는 함수.
	void SetAnimation( ClipType clipType, bool isLoop ){
//		skAnimation.state.ClearTrack(0);

		if( characterType == KDIE_CHARACTER_TYPE.GENERAL_TYPE )
			skAnimation.state.SetAnimation(0,clipType.ToString(),isLoop);
//			skAnimation.state.AddAnimation(0,"idle",isLoop,0);
//		else 
//			skAnimation.state.SetAnimation(0,attackType.ToString() + "_" + clipType.ToString(),isLoop);

//		skAnimation.skeleton.SetSlotsToSetupPose();
	}
	// moveVector의 값에 따라 애니메이션 처리를 어찌 할 것인가 결정 해주는 함수. 
	void AutoSetAniState(float magnitude){
		if( isOneShotAnimationning ) return;

		if( magnitude > 0 && currentClipType != ClipType.run)
		{
			SetClip( ClipType.run );
		}
		else if( magnitude <= 0 && currentClipType != ClipType.idle )
		{
			SetClip( ClipType.idle );
		}
	}

	string GetAttackType(){
		return "";
	}

}
