﻿using UnityEngine;
using System.Collections;
using Spine;

public class SpineExtendCharacterBulletMaker : MonoBehaviour {

	public TestBullet bulletPrefab;
	public bool IsShotted{get;set;}

	public BoneBoundingBox weaponBone;

	public float imageBaseRotation = 270f;

	public void ActionBulletMaking( BoneBoundingBox weaponBoundingBox ){

		IsShotted = false;

		weaponBone = weaponBoundingBox;

		name = weaponBoundingBox.name;

		StartCoroutine( ActionBulletMakingIE() );
	}

	IEnumerator ActionBulletMakingIE(){

		yield return new WaitForSeconds(0f);

		while( ! IsShotted ){

			if(! IsShotted && weaponBone.transform.localScale.x > 0 ){
				IsShotted = true;
				
				TestBullet bullet = Instantiate( bulletPrefab) as TestBullet;

				if( transform.eulerAngles.y == 0 ){
					bullet.Direct = true;
				}else{
					bullet.Direct = false;
				}

				bullet.transform.position = weaponBone.transform.position;
				bullet.transform.rotation = weaponBone.transform.rotation;
			}

			yield return new WaitForSeconds(0f);
		}
	}
}
