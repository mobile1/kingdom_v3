﻿using UnityEngine;
using System.Collections;

public class SpineTestTarget : MonoBehaviour {
	public ParticleSystem effect;
	public Transform[] path;
	// Use this for initialization
	void Start () {
		if( effect != null ) effect.Stop();

		if( path.Length > 1 ) iTween.MoveTo(gameObject, iTween.Hash("path",path,"speed",1f,"looptype", iTween.LoopType.pingPong, "easetype", iTween.EaseType.linear));
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnCollisionEnter2D(Collision2D coll) {
		ActionRotate();
	}

	void OnTriggerEnter2D(Collider2D other) {
		ActionRotate();
	}

	void ActionRotate(){
		effect.Play();
		iTween.RotateAdd( this.gameObject, iTween.Hash( "z", 720f,"time",2f, "oncomplete", "ActionRotateEnd"));
	}
	void ActionRotateEnd(){
		effect.Stop();
	}
}
