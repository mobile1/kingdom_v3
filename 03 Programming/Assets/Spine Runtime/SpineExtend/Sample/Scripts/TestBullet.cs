﻿using UnityEngine;
using System.Collections;

public class TestBullet : MonoBehaviour {
	public float speed;
	public bool Direct{ get; set; }

	// Use this for initialization
	void Start () {
		StartCoroutine (ShotProc());	
	}
	
	// Update is called once per frame
	void Update () {
		if( isMove ){
			Vector3 pos = transform.position;
			pos.x += speed * (Direct ? 1 : -1) * Time.deltaTime;
			transform.position = pos;
		}
	}

	bool isMove = false;
	IEnumerator ShotProc(){
		yield return new WaitForSeconds(0f);

		isMove = true;

		yield return new WaitForSeconds(5f);

		isMove = false;

		DestroyObject (this.gameObject);
	}
}
