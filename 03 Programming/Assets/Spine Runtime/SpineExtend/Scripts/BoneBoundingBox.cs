﻿using UnityEngine;
using System.Collections;
using Spine;
using System;

//[ExecuteInEditMode]
public class BoneBoundingBox : MonoBehaviour {
	public SkeletonAnimationExtend skAnimation;
	public int BoneIdx{get;set;}

	public Spine.Bone bone;
	 
	// 본 이름, 이미지 이름.
	public string boneName,spriteName;
	// 이미지의 기본 방향값을 잡아 준다. 프로그램 월드는 우측이 0도 이나 그래픽팀은 위쪽을 0도로 잡아 이미지를 구현된 관계로 -90도 틀어 준다.
	public float imageBaseRotation = 270f;

	public BoxCollider2D boxCollider2D;

//  몸체 충돌영역 으로 사용 할 경우 실시간 싱크가 필요 없어 플래그를 달아 둔다.
//	public bool useRealTimeBoundingUpdate = true;

	// Use this for initialization
	void Start () {
//		print(gameObject.name + " "  + boneName + " " + spriteName);
		// 바운딩 영력 잡기 초기화.
		if( bone == null ) 	InitBoundingBox ( boneName, spriteName, true );
	}
	
	// Update is called once per frame
	void Update () {
//		if (bone != null && useRealTimeBoundingUpdate )	SyncherProc ();
		// 본데이터를 읽어 실시간으로 위치르 잡아 준다.
		if (bone != null  )	SyncherProc ();
	}
	// 기본 콤포넌트 생성.
	public void CreateBoxCollider(){

		boxCollider2D = gameObject.AddComponent<BoxCollider2D>() as BoxCollider2D;
		gameObject.AddComponent< Rigidbody2D>();

		// Setting RigidBody
		rigidbody2D.gravityScale = 0;
		rigidbody2D.fixedAngle = true;
		boxCollider2D.isTrigger = true;
	}
	// 이미지 크기에 맞춰 충돌 영역을 설정 한다.
	public void ReBounds(string name){

        if (boxCollider2D.enabled == false) return;

		Bounds bounds;

		try{
			bounds = skAnimation.skeletonDataAsset.spriteCollection.GetSpriteDefinition( name ).GetBounds();

			if( boxCollider2D == null ) CreateBoxCollider();
			
			boxCollider2D.size = bounds.size;
			boxCollider2D.center = bounds.center;

		}catch( NullReferenceException e ){
			print( "Error : Not Found sprite name " + name + "\n" + e.ToString());
		}
	}

	public void EnableBoxCollider2D(bool _value){
		boxCollider2D.enabled = _value;
	}

	// 충돌 영역을 잡기 위해 본의 이름, 이미지 이름을 가지고 영역을 잡는다.
	public void InitBoundingBox(string _boneName, string _spriteName, bool enableBoxCollider){
	
//		print(gameObject.name + " "  + _boneName + " " + _spriteName);

        
        //gameObject.name = 
		boneName = _boneName;
		spriteName = _spriteName;


		if (skAnimation != null)
		{
			bone = skAnimation.skeleton.FindBone( boneName );

			if( bone == null ) print ("Error : Not found boneName " + boneName );

			ReBounds( spriteName );

			SyncherProc();

			skAnimation.skeleton.SetAttachment( boneName, null );

			boxCollider2D.enabled = enableBoxCollider;
		}
		else{
			print ( "Error Skeletone Animamtion ");
		}
	}
	// 실시간으로 충돌 영역의 위치를 동기화 시킨다.
	void SyncherProc(){

		Transform parentTF = skAnimation.transform;
		Vector3 basePos = parentTF.position;

		// Check Flip by Rotation
		// if( ! skAnimation.IsFlip )
		if( parentTF.eulerAngles.y == 0 )
		{
			transform.localPosition = new Vector3 (bone.WorldX,bone.WorldY, transform.localPosition.z);
//			transform.localRotation = Quaternion.Euler (0, parentTF.eulerAngles.y, bone.WorldRotation + imageBaseRotation);
			transform.localRotation = Quaternion.Euler (0, 0, bone.WorldRotation + imageBaseRotation);
			transform.localScale	= new Vector3 (bone.WorldScaleX, bone.WorldScaleY, transform.localScale.z );
		}
		else
		{
			transform.position = basePos + new Vector3 (- bone.WorldX, bone.WorldY, transform.localPosition.z);
//			transform.localRotation = Quaternion.Euler (0, parentTF.eulerAngles.y, bone.WorldRotation + imageBaseRotation);
			transform.rotation = Quaternion.Euler (0, 180, bone.WorldRotation + imageBaseRotation);
			transform.localScale	= new Vector3 (bone.WorldScaleX, bone.WorldScaleY, transform.localScale.z );
		}

	}

	// 스켈레톤 데이터등이 변경 될시 외부 클래스에서 본 함수를 통해 설정 한다.
	public void SetSkeletoneAnimationExtend( SkeletonAnimationExtend ske, string _boneName, string _spriteName ){
		skAnimation = ske;

		//print (_boneName + "  " + _spriteName );
		CreateBoxCollider();

		SyncherProc();
	}


}
