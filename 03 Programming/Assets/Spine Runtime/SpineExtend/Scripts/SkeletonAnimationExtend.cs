﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[ExecuteInEditMode]
[AddComponentMenu("Spine/SkeletonAnimationExtend")]
public class SkeletonAnimationExtend : SkeletonAnimation {
	// 무기 체크용 바운딩.
	// 쌍검이 나올 경우를 대비 일단 리스트로 작성.
	public List< BoneBoundingBox > weaponBoundingBoxs = new List<BoneBoundingBox>();
	// 바디 체킹용 바운딩.
	public BoneBoundingBox bodyBoundingBox;

	// 외부에서 본 클래스의 스파인 데이터 교체시 본 함수를 통해 진행 한다.
	public void SetSkeletonDataAsset( SkeletonDataAsset data){
		this.skeletonDataAsset = data;
		// 스파인 데이터가 교체되면 바로 네임 값을 널로 해야 에러가 않남.
		this.initialSkinName = "";
		this.AnimationName = "";
		// 리셋이 매우 중요함. 안하면 뻑남.
		this.Reset();
	}

	// 배운딩 본을 만들어 내는 함수.
	public void CreateWeaponBoundingBox(){
		GameObject go  = new GameObject("Bone");
		go.transform.parent = transform;
		go.transform.localPosition = Vector3.zero;
		weaponBoundingBoxs.Add( go.AddComponent<BoneBoundingBox>() as BoneBoundingBox );

		int idx = weaponBoundingBoxs.Count -1;

		weaponBoundingBoxs[ idx].skAnimation = this;

		string boneName = skeleton.Bones[0].Data.name;
		string spriteName = this.skeletonDataAsset.spriteCollection.spriteDefinitions[0].name;

		weaponBoundingBoxs[ idx].InitBoundingBox( boneName, spriteName , true);

//		weaponBoundingBoxs[ idx].useRealTimeBoundingUpdate = true;
	}

	public void DeleteWeaponBoundingBox(int idx){
		if( weaponBoundingBoxs.Count == 0 ) return;
		if( weaponBoundingBoxs[ idx ] != null ) DestroyImmediate( weaponBoundingBoxs[ idx ].gameObject , true );
		weaponBoundingBoxs.RemoveAt( idx );
	}
	// 바디 바운딩.
	public void CreateBodyBoundingBox(){
		GameObject go = new GameObject("Body");
		go.transform.parent = transform;
		go.transform.localPosition = Vector3.zero;
		bodyBoundingBox = go.AddComponent<BoneBoundingBox>() as BoneBoundingBox;

		bodyBoundingBox.skAnimation = this;

//		string boneName = skeleton.Bones[0].Data.name;
//		string spriteName = this.skeletonDataAsset.spriteCollection.spriteDefinitions[0].name;

		bodyBoundingBox.InitBoundingBox( "Body", "Body", true );

//		bodyBoundingBox.useRealTimeBoundingUpdate = false;
	}

	public void DeleteBodyBoundingBox(){
		if( bodyBoundingBox != null ) DestroyImmediate( bodyBoundingBox.gameObject , true );
		bodyBoundingBox = null;
	}

	// 스파인의 컬러를 변경 시키는 함수.
	public Color m_color = Color.white;
	public Color color{
		get{return m_color;}
		set{ 
			if( skeleton != null ){
				skeleton.R = m_color.r = value.r;
				skeleton.G = m_color.g = value.g;
				skeleton.B = m_color.b = value.b;
				skeleton.A = m_color.a = value.a;
			}else{
				m_color = value;
			}
		}
	}
	
	public float R{ set{ skeleton.R = value; }}
	public float G{ set{ skeleton.G = value; }}
	public float B{ set{ skeleton.B = value; }}
	public float A{ set{ skeleton.A = value; }}

	// 컬러 페이딩 함수 연동. 현재 진행 중인가.
	public bool isFading;
	// 주어진 시간동안 알파 값이 빠져 버린다.
	public void ActionFadeTo(float time, float alpha){ 
		if( ! isFading ) StartCoroutine (ActionFadeToIE (time, alpha));
	}
	IEnumerator ActionFadeToIE(float time, float alpha){
		isFading = true;
		float timeinc = 0;
		Color tColor = new Color(skeleton.R,skeleton.G,skeleton.B,skeleton.A);
		
		if( alpha > 1) alpha = 1f;
		if( alpha < 0) alpha = 0f;
		
		float a = alpha - tColor.a;
		float checkTime = 0.1f;
		
		while( timeinc < time){
			// 시간 증가량 * 범위 / 전체 시간
			skeleton.A += checkTime * a / time;
			
			timeinc += checkTime;
			
			yield return new WaitForSeconds (checkTime);
		}
		
		skeleton.A = alpha;
		
		isFading = false;
	}
	
	// 주어진 시간동안 컬러 값이 빠져 버린다.
	public void ActionColorTo(float time, Color color){ 
		if( ! isFading ) StartCoroutine (ActionColorToIE (time, color));
	}
	IEnumerator ActionColorToIE(float time, Color color){
		isFading = true;
		
		float timeinc = 0;
		Color tColor = new Color(skeleton.R,skeleton.G,skeleton.B,skeleton.A);
		float r = color.r - tColor.r;
		float g = color.g - tColor.g;
		float b = color.b - tColor.b;
		float a = color.a - tColor.a;
		
		float checkTime = 0.1f;
		
		while( timeinc < time){
			
			skeleton.R += checkTime * r / time;
			skeleton.G += checkTime * g / time;
			skeleton.B += checkTime * b / time;
			skeleton.A += checkTime * a / time;
			
			timeinc += checkTime;
			
			yield return new WaitForSeconds (checkTime);
		}
		
		skeleton.R = color.r;
		skeleton.G = color.g;
		skeleton.B = color.b;
		skeleton.A = color.a;
		
		isFading = false;
	}


    //  추가
    public void ActionColor(Color _color)
    {
        skeleton.R = _color.r;
        skeleton.G = _color.g;
        skeleton.B = _color.b;
        skeleton.A = _color.a;
    }

    //  추가
    public void ActionFade(float _alpha)
    {
        skeleton.A = color.a;
    }
}
