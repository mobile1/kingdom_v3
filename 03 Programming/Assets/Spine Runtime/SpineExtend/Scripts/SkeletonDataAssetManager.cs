﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SkeletonDataAssetManager : MonoSingleton<SkeletonDataAssetManager> {
	// 캐릭터 타입 정의.
	//public enum CharacterType{ General, Soldier, Max };

	//public enum Group{A,B,C,D,E,Y,Max};

	public List< SkeletonDataAsset> generalCodeA = new List<SkeletonDataAsset>();
	public List< SkeletonDataAsset> generalCodeB = new List<SkeletonDataAsset>();
	public List< SkeletonDataAsset> generalCodeC = new List<SkeletonDataAsset>();
	public List< SkeletonDataAsset> generalCodeD = new List<SkeletonDataAsset>();
	public List< SkeletonDataAsset> generalCodeE = new List<SkeletonDataAsset>();
	public List< SkeletonDataAsset> generalCodeY = new List<SkeletonDataAsset>();

	public List< SkeletonDataAsset> soldierCodeA = new List<SkeletonDataAsset>();
	public List< SkeletonDataAsset> soldierCodeB = new List<SkeletonDataAsset>();
	public List< SkeletonDataAsset> soldierCodeC = new List<SkeletonDataAsset>();
	public List< SkeletonDataAsset> soldierCodeD = new List<SkeletonDataAsset>();

    public List<SkeletonDataAsset> GetCharacterSKData(KDIE_CHARACTER_TYPE character_type, KDIE_ATTACK_TYPE group)
    {
        if (character_type == KDIE_CHARACTER_TYPE.GENERAL_TYPE)
		{
			switch( group ){
                case KDIE_ATTACK_TYPE.SPEAR_TYPE: 
                    return generalCodeA; break;
                case KDIE_ATTACK_TYPE.SWORD_TYPE: 
                    return generalCodeB; break;
                case KDIE_ATTACK_TYPE.BOW_TYPE: 
                    return generalCodeC; break;
                case KDIE_ATTACK_TYPE.THROW_TYPE: 
                    return generalCodeD; break;
                case KDIE_ATTACK_TYPE.MACE_TYPE:
                    return generalCodeE; break;
                case KDIE_ATTACK_TYPE.FAN_TYPE:
                    return generalCodeY; break;
			    default : 		
                    return generalCodeA; break;
			}
		}
		else
		{
			switch( group ){
                case KDIE_ATTACK_TYPE.SPEAR_TYPE: 
                    return soldierCodeA; break;
                case KDIE_ATTACK_TYPE.SWORD_TYPE:
                    return soldierCodeB; break;
                case KDIE_ATTACK_TYPE.BOW_TYPE:
                    return soldierCodeC; break;
                //			case KDIE_ATTACK_TYPE.D:	return soldierCodeD; break;
			default : 		
                return soldierCodeA; break;
			}
		}
	}


	string[] weaponCodeGA = new string[]{"GAW001","GAW002","GAW003","GAW004","GAW005","GAW006"};
	string[] weaponCodeGB = new string[]{"GBW001","GBW002","GBW003","GBW004","GBW005","GBW006"};
	string[] weaponCodeGC = new string[]{"GCW001","GCW002","GCW003","GCW004","GCW005","GCW006"};// weapon is equal bullet
	string[] weaponCodeGD = new string[]{"GDW001","GDW002","GDW003","GDW004","GDW005","GDW006"};
	string[] weaponCodeGE = new string[]{"GEW001","GEW002","GEW003","GEW004","GEW005","GEW006"};
	string[] weaponCodeGY = new string[]{"GYB001"};

	string[] weaponCodeSA = new string[]{"SAW001","SAW002","SAW003","SAW004","SAW005"};
	string[] weaponCodeSB = new string[]{"SBW001","SBW002","SBW003","SBW004","SBW005"};
	string[] weaponCodeSC = new string[]{"SCW001","SCW002","SCW003","SCW004","SCW005"};
//	string[] weaponCodeSD = new string[]{"GDW001","GDW002","GDW003","GDW004","GDW005"};

    void Awake()
    {
        DontDestroyOnLoad(this);
    }


    public string[] GetWeaponCode(KDIE_CHARACTER_TYPE character_type, KDIE_ATTACK_TYPE group)
    {
        if (character_type == KDIE_CHARACTER_TYPE.GENERAL_TYPE)
		{
			switch( group ){
                case KDIE_ATTACK_TYPE.SWORD_TYPE: return weaponCodeGA; break;
                case KDIE_ATTACK_TYPE.SPEAR_TYPE: return weaponCodeGB; break;
                case KDIE_ATTACK_TYPE.BOW_TYPE: return weaponCodeGC; break;
                case KDIE_ATTACK_TYPE.THROW_TYPE: return weaponCodeGD; break;
                case KDIE_ATTACK_TYPE.MACE_TYPE: return weaponCodeGE; break;
                case KDIE_ATTACK_TYPE.FAN_TYPE: return weaponCodeGY; break;
			default : 		return weaponCodeGA; break;
			}
		}
		else
		{
			switch( group ){
                case KDIE_ATTACK_TYPE.SWORD_TYPE: return weaponCodeSA; break;
                case KDIE_ATTACK_TYPE.SPEAR_TYPE: return weaponCodeSB; break;
                case KDIE_ATTACK_TYPE.BOW_TYPE: return weaponCodeSC; break;
//			case Group.D:	return weaponCodeSD; break;
			default : 		return weaponCodeSA; break;
			}
		}
	}

	string[] bulletCodeGA = new string[]{};//,"GAW002","GAW003","GAW004","GAW005"};
	string[] bulletCodeGB = new string[]{};//,"GBW002","GBW003","GBW004","GBW005"};
	string[] bulletCodeGC = new string[]{"GCB001"};//,"GCB002","GCB003","GCB004","GCB005"};// weapon is equal bullet
	string[] bulletCodeGD = new string[]{"GDW001","GDW002","GDW003","GDW004","GDW005"};
	string[] bulletCodeGE = new string[]{"GEW001","GEW002","GEW003","GEW004","GEW005"};
	string[] bulletCodeGY = new string[]{"GYB001"};


// 	[System.Serializable]
// 	public class ObjectFolder{
// 		[HideInInspector]
// 		public string name;
// 		public Object folder;
// 		public ObjectInfo[] objectInfos;
// 	}
//	
// 	[System.Serializable]	
// 	public class ObjectInfo{		
// 		[HideInInspector]
// 		public string name;
// 		public GameObject prefab;
//		
// 		//Construct
// 		public ObjectInfo(GameObject go){
// 			prefab = go;
// 			name = prefab.name;			
// 		}
// 	}
//	
// 	public Dictionary<string, SkeletonDataAsset> collection = new Dictionary<string, SkeletonDataAsset>();
//	
// 	public ObjectFolder[] folders;
//	
//
//
// 	// Use this for initialization
// 	void Start () {
	
// 	}
	
// 	// Update is called once per frame
// 	void Update () {
	
// 	}

// 	public void CreateObject(string key){
// //		collection[key].AddCreateObject(true,Vector3.zero);
// 	}

// 	public void CreateCollection(){
		
// 		//			collections_debug.Clear();
		
// //		collections.Clear();
		
// //		for(int fid = 0; fid < folders.Length; fid++){
// //			foreach ( GPPrefabInfo info in folders[fid].prefabInfos ){
// //				GPCollection collection = new GPCollection( info );	
// //				
// //				//				collections_debug.Add( collection );
// //				collections.Add(  info.prefab.name, collection );
// //			}
// //		}
// 	}
}
